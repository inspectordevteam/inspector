package ru.sergeymaslov.inspector;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.Tools.DateTools;

/**
 * Created by Администратор on 28.02.2016.
 */
public class FileTools {
    public static final String TAG = "FileTools";
    public static final String MAIN_DIRECTORY_NAME = "Inspector";
    public static final boolean copyFile(String src, String dst) {
        try {
            copy(src, dst);
            return true;
        } catch (Exception e) {
            //Logger.warn(TAG, "Copy failed", e);
            return false;
        }
    }

    public static File createEventDirectory(String eventDirectoryName){
        File newEventDirectory = createDirectory(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + MAIN_DIRECTORY_NAME, eventDirectoryName);
        if (newEventDirectory != null) {
            Log.i(TAG, "event directory name " + eventDirectoryName + " created successfully");
            return newEventDirectory;
        }else {
            Log.e(TAG, "could not create event directory name " + eventDirectoryName);
            return null;
        }
    }

    private static File createDirectory(String path, String name){
        File newDirectory = new File(path, name);
        if (!newDirectory.exists()){
            newDirectory.mkdirs();
        }
        return newDirectory;
    }

    public static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = DateTools.dateToString(new Date(), "yyyyMMdd_HHmmss");
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStorageDirectory(), MAIN_DIRECTORY_NAME);
        if (!storageDir.exists()){
            if (storageDir.mkdirs()){
                Log.i(TAG, "storage created successfully");
            }
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        /*mCurrentPhotoPath = "file:" + image.getAbsolutePath();*/
        return image;
    }

    public static void moveFilesFromOneDirectoryToAnother(String sourceDirectory, ArrayList<String> filesNames, File destDirectory){
        InputStream in = null;
        OutputStream out = null;
        try {
            for (String fileName : filesNames) {
                in = new FileInputStream(sourceDirectory + File.separator + fileName);
                out = new FileOutputStream(destDirectory.getAbsolutePath() + File.separator + fileName);

                byte[] buffer = new byte[1024];
                int read;
                while ((read = in.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }
                in.close();

                // write the output file
                out.flush();
                out.close();

                // delete the original file
                new File(sourceDirectory + File.separator + fileName).delete();
            }

        } catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public static File getEventFolder(String eventId){
        File eventFolder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                File.separator + MAIN_DIRECTORY_NAME + File.separator + eventId);
        Log.d(TAG, "EventFolder file with eventId " + eventId + (eventFolder.isDirectory() ? " is a directory" : " is not a directory"));
        Log.d(TAG, "EventFolder file with eventId " + eventId + (eventFolder.exists() ? " exists" : " does not exist"));
        return eventFolder;
    }

    public static String generateEventFolderName(long eventId, String driverInfo){
        return String.valueOf(eventId) + StringConstants.UNDERLINE + driverInfo;
    }

    public static void checkIfDatabaseFolderExists(){
        String path = InspectorApplication.getInstance().getDatabaseInternalPath().toString().replace(File.separator + InspectorApplication.DATABASE_FILE_NAME, StringConstants.EMPTY);
        File databaseFolder = new File(path);
        if (!databaseFolder.exists()){
            if (databaseFolder.mkdirs()){
                Log.d(TAG, "Database folder has been successfully created");
            } else {
                Log.d(TAG, "Database folder was not created");
            }
        }
    }

    public static void checkIfDatabaseFileExists(){
        File dstFile = new File(InspectorApplication.getInstance().getDatabaseInternalPath());
        if (!dstFile.exists()) {
            try {
                dstFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static final void copy(String src, String dst) throws IOException {
        BufferedInputStream is = null;
        BufferedOutputStream os = null;
        try
        {
            is = new BufferedInputStream(new FileInputStream(src));
            os = new BufferedOutputStream(new FileOutputStream(dst));

            // копирование файла
            byte data[] = new byte[2048];
            int count;
            while ((count = is.read(data, 0, 2048)) != -1) {
                os.write(data, 0, count);
            }
        }
        finally
        {
            if (os != null) {
                os.flush();
                os.close();
            }
            if (is != null) is.close();
        }
    }

    public static byte[] convertFileByteArray(File fileToConvert){
        if (!fileToConvert.exists()) return null;
        byte[] bFile = new byte[(int) fileToConvert.length()];
        try {
            BufferedInputStream fis = new BufferedInputStream(new FileInputStream(fileToConvert));
            fis.read(bFile);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bFile;
    }
}
