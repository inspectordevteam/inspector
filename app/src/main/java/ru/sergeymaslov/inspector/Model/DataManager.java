package ru.sergeymaslov.inspector.Model;

/**
 * Created by Администратор on 08.04.2016.
 */
public class DataManager {
    private IDataProvider mDataProvider;

    public DataManager(IDataProvider dataProvider){
        mDataProvider = dataProvider;
    }

    public IDataProvider getDataProvider(){
        return mDataProvider;
    }
}
