package ru.sergeymaslov.inspector.Model;

import java.util.Date;
import java.util.List;

import ru.sergeymaslov.inspector.Car;
import ru.sergeymaslov.inspector.Driver;
import ru.sergeymaslov.inspector.Event;
import ru.sergeymaslov.inspector.Violation;
import rx.Observable;

/**
 * Created by Администратор on 08.04.2016.
 */
public interface IDataProvider {
    Observable<List<Driver>> getDrivers();
    Observable<List<Car>> getCars();
    Observable<List<Event>> getEvents();
    Observable<List<Violation>> getViolations();
    Observable deleteObject(Object obj);
    Date getLastSyncDateFor(String key);
}
