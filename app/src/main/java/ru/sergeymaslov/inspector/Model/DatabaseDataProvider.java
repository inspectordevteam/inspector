package ru.sergeymaslov.inspector.Model;



import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;
import java.util.List;

import ru.sergeymaslov.inspector.Car;
import ru.sergeymaslov.inspector.Driver;
import ru.sergeymaslov.inspector.Event;
import ru.sergeymaslov.inspector.Event_Table;
import ru.sergeymaslov.inspector.LastSyncDate;
import ru.sergeymaslov.inspector.LastSyncDate_Table;
import ru.sergeymaslov.inspector.Violation;
import rx.Observable;

/**
 * Created by Администратор on 08.04.2016.
 */
public class DatabaseDataProvider implements IDataProvider {

    @Override
    public Observable<List<Driver>> getDrivers() {
        Observable<List<Driver>> observable = Observable.create(subscriber -> {
            subscriber.onNext(new Select().from(Driver.class).queryList());
        });
        return observable;
    }

    @Override
    public Observable<List<Car>> getCars() {
        Observable<List<Car>> observable = Observable.create(subscriber -> {
                subscriber.onNext(new Select().from(Car.class).queryList());
        });
        return observable;
    }

    @Override
    public Observable<List<Event>> getEvents() {
        Observable<List<Event>> observable = Observable.create(subscriber -> {
            subscriber.onNext(new Select().from(Event.class).orderBy(Event_Table.eventDate, false).queryList());
        });
        return observable;
    }

    @Override
    public Observable<List<Violation>> getViolations() {
        Observable<List<Violation>> observable = Observable.create(subscriber -> {
            subscriber.onNext(new Select().from(Violation.class).queryList());
        });
        return observable;
    }

    @Override
    public Observable deleteObject(Object obj) {
        BaseModel model = (BaseModel) obj;
        Observable observable = Observable.create(subscriber -> {
            model.delete();
            subscriber.onCompleted();
        });
        return observable;
    }

    @Override
    public Date getLastSyncDateFor(String key) {
        LastSyncDate lsd = new Select().from(LastSyncDate.class).where(Condition.column(LastSyncDate_Table.name.getNameAlias()).is(key)).querySingle();
        return lsd != null ? lsd.getDate() : null;
    }
}
