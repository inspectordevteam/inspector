package ru.sergeymaslov.inspector;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by Администратор on 25.02.2016.
 */
@Database(name = InspectorDatabase.NAME, version = InspectorDatabase.VERSION)
public class InspectorDatabase {
    public static final String NAME = "InspectorDatabase";
    public static final int VERSION = 1;
}
