package ru.sergeymaslov.inspector.Dialogs;

import android.app.*;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.sergeymaslov.inspector.Interface.Lists.ValueChooserListItem;
import ru.sergeymaslov.inspector.Presenter.AddDriverDialogPresenter;
import ru.sergeymaslov.inspector.R;
import ru.sergeymaslov.inspector.View.IAddDriverDialogView;
import ru.sergeymaslov.inspector.View.IView;

/**
 * Created by Maslov on 21.04.2016.
 */
public class AddDriverDialog extends DialogFragment implements IAddDriverDialogView {
    @Bind(R.id.driverName)protected EditText driverName;
    @Bind(R.id.driverLastname)protected EditText driverLastName;
    @Bind(R.id.driverSurname)protected EditText driverSurName;
    @Bind(R.id.driverLicenseNumber)protected EditText driverLicenseNumber;

    private AddDriverDialogPresenter mPresenter;
    private ValueChooserListItem mValueChooserListItem;

    public AddDriverDialog() {

    }
    public void setValueChooserListItem(ValueChooserListItem item){
        mValueChooserListItem = item;
    }
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        View layout = getActivity().getLayoutInflater().inflate(R.layout.add_driver_layout, null);
        mPresenter = new AddDriverDialogPresenter();
        mPresenter.bindActivity(this);
        ButterKnife.bind(this, layout);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        DialogInterface.OnClickListener clickListener = (dialog, which) -> {
            String driverName = this.driverName.getText().toString();
            String driverSurname = this.driverSurName.getText().toString();
            String driverLastname = this.driverLastName.getText().toString();
            String driverLicenseNumber = this.driverLicenseNumber.getText().toString();
            mPresenter.saveDriver(driverName, driverSurname, driverLastname, driverLicenseNumber, mValueChooserListItem);
        };
        builder.setView(layout);
        builder.setPositiveButton(getString(R.string.btn_ok), clickListener);

        DialogInterface.OnClickListener negativeClick = (dialog, which) ->
                dialog.dismiss();

        builder.setNegativeButton(getString(R.string.btn_cancel), negativeClick);
        return builder.show();
    }

    @Override
    public void onDestroyView() {
        mPresenter.unbindActivity();
        super.onDestroyView();
    }
}
