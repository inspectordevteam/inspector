package ru.sergeymaslov.inspector;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;
import java.util.List;

import ru.sergeymaslov.inspector.Annotations.Sync;
import ru.sergeymaslov.inspector.BaseClasses.IName;
import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.Interfaces.Retrievable;
import ru.sergeymaslov.inspector.Interfaces.Syncable;

import ru.sergeymaslov.inspector.Syncronization.DataSync;
import ru.sergeymaslov.inspector.Syncronization.SyncStatusIds;

/**
 * Created by Администратор on 25.02.2016.
 */
@Table(database = InspectorDatabase.class)
public class Driver extends BaseModel implements IName, Syncable, Retrievable {

    @Sync
    @PrimaryKey
    public long driverId;

    @Sync
    @Column
    public String driverLicenseNumber;

    @Sync
    @Column
    public String name;

    @Sync
    @Column
    public String surname;

    @Sync
    @Column
    public String lastName;

    @Column
    private int syncStatus;

    public int getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(int syncStatus) {
        this.syncStatus = syncStatus;
    }

    List<Event> events;
    List<Car> cars;

    public Driver(){}

    public List<Event> getEvents(){
        if (events == null){
           events = new Select().from(Event.class).where(Condition.column(Event_Table.driverId.getNameAlias()).is(this.driverId)).queryList();
        }
        return events;
    }

    public long getDriverId(){
        return driverId;
    }

    public void setDriverId(long driverId){
        this.driverId = driverId;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String getObjectName() {
        return lastName + StringConstants.SPACE + name + StringConstants.SPACE + surname;
    }

    @Override
    public String getMessage() {
        return InspectorApplication.getInstance().getString(R.string.drivers);
    }

    @Override
    public void onSyncFinished() {
        syncStatus = SyncStatusIds.STATUS_SENT;
        save();
    }

    @Override
    public String getPath() {
        return "save-drivers";
    }

    @Override
    public String getKeyName() {
        return null;
    }
}
