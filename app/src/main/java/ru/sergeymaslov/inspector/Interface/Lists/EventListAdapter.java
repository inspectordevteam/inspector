package ru.sergeymaslov.inspector.Interface.Lists;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.sergeymaslov.inspector.R;
import ru.sergeymaslov.inspector.StringConstants;
import ru.sergeymaslov.inspector.Violation;

/**
 * Created by Администратор on 28.02.2016.
 */
public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.ViewHolder>{
    private List<EventListItem> mItems = new ArrayList<>();
    private Context mContext;
    private View.OnClickListener mItemClickListener;
    public EventListAdapter(Context context){
        mContext = context;
    }

    public Object getItem(int position) {
        return mItems.get(position);
    }

    public void setItems(List<EventListItem> items){
        mItems.clear();
        mItems.addAll(items);
    }

    public void setOnItemClickListener(View.OnClickListener itemClickListener){
        mItemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(mContext).inflate(R.layout.event_item_layout, parent, false);
        return new ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EventListItem item = (EventListItem) getItem(position);
        holder.itemView.setBackgroundColor(position % 2 == 0 ? mContext.getResources().getColor(R.color.material_brown_50) : mContext.getResources().getColor(android.R.color.white));
        holder.eventDriver.setText(item.getDriverFIO());
        holder.eventCar.setText(item.getCarName());
        String eventDate = item.getEventDate();
        holder.eventDate.setText(item.getEventDate());
        holder.violation.setText(appendAllValuesInOneString(item));

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    private String appendAllValuesInOneString(EventListItem item){
        StringBuffer buffer = new StringBuffer();
        for (Violation value : item.getViolations()){
            buffer.append(value.getObjectName());
            buffer.append(StringConstants.NEWLINE);
        }
        return buffer.toString();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.eventDriver)TextView eventDriver;
        @Bind(R.id.eventCar)TextView eventCar;
        @Bind(R.id.eventDate)TextView eventDate;
        @Bind(R.id.violation)TextView violation;
        @Bind(R.id.violationsContainer)RelativeLayout violationsContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(mItemClickListener);
            ButterKnife.bind(this, itemView);
        }
    }
}
