package ru.sergeymaslov.inspector.Interface.Lists;

import java.util.ArrayList;

import ru.sergeymaslov.inspector.Violation;

/**
 * Created by Администратор on 28.02.2016.
 */
public class EventListItem implements IListItem{
    private String mEventDate;
    private String driverFIO;
    private ArrayList<Violation> violations;
    private String carName;

    public EventListItem(String mEventDate, String driverFIO, String carName) {
        this.mEventDate = mEventDate;
        this.driverFIO = driverFIO;
        this.carName = carName;
    }

    public String getEventDate() {
        return mEventDate;
    }

    public void setEventDate(String mEventDate) {
        this.mEventDate = mEventDate;
    }

    public String getDriverFIO() {
        return driverFIO;
    }

    public void setDriverFIO(String driverFIO) {
        this.driverFIO = driverFIO;
    }

    public ArrayList<Violation> getViolations() {
        return violations;
    }

    public void setViolations(ArrayList<Violation> violations) {
        this.violations = violations;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public boolean hasFieldWithText(CharSequence sequence){
        return mEventDate.toLowerCase().contains(sequence) ||
                driverFIO.toLowerCase().contains(sequence) ||
                isViolationsContainText(sequence) ||
                carName.toLowerCase().contains(sequence);
    }

    private boolean isViolationsContainText(CharSequence sequence){
        for (Violation violation : getViolations()){
            return violation.getViolationText().toLowerCase().contains(sequence);
        }
        return false;
    }

    @Override
    public void onClick() {

    }
}
