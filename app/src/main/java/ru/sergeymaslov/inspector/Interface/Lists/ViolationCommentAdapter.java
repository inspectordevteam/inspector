package ru.sergeymaslov.inspector.Interface.Lists;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ru.sergeymaslov.inspector.R;
import ru.sergeymaslov.inspector.Violation;

/**
 * Created by Администратор on 15.03.2016.
 */
public class ViolationCommentAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<Violation> mItems = new ArrayList<>();

    public ViolationCommentAdapter(Context context) {
        mContext = context;
    }

    public void setItems(ArrayList<Violation> items){
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_comment_dialog_item, null);
            holder.violationName = (TextView) convertView.findViewById(R.id.violationLabel);
            holder.comment = (TextView) convertView.findViewById(R.id.violationValue);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Violation item = (Violation) getItem(position);
        holder.violationName.setText(item.getObjectName());
        if (item.comment != null) {
            holder.comment.setHint(item.comment);
        }
        return convertView;
    }

    class ViewHolder{
        TextView violationName;
        TextView comment;
    }
}
