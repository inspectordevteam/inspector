package ru.sergeymaslov.inspector.Interface.Lists;

import java.util.ArrayList;

/**
 * Created by Администратор on 28.02.2016.
 */
public abstract class ValueChooserListItem implements IListItem {
    private String caption;
    private String defaultValue;
    private ArrayList<String> mSelectedValues;
    public ValueChooserListItem(String caption, String selectedValue){
        this.caption = caption;
        this.defaultValue = selectedValue;
        mSelectedValues = new ArrayList<>();
    }

    public String getCaption() {
        return caption;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void addToSelectedValues(String value, boolean append){
        if (!append) {
            mSelectedValues.clear();
        }
        mSelectedValues.add(value);
    }
    public void removeFromSelectedValues(String value){
        mSelectedValues.remove(value);
    }

    public ArrayList<String> getSelectedValues() {
        return mSelectedValues;
    }
}
