package ru.sergeymaslov.inspector.Interface.Lists;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import ru.sergeymaslov.inspector.R;
import ru.sergeymaslov.inspector.StringConstants;

/**
 * Created by Администратор on 28.02.2016.
 */
public class ValueChooserListAdapter extends BaseRecyclerViewAdapter<ValueChooserListItem> {

    public ValueChooserListAdapter(Context context){
       super(context);
    }

    @Override
    public ValueChooserListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.new_event_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder)holder;
        ValueChooserListItem item = (ValueChooserListItem) getItem(position);
        viewHolder.selectedValue.setText(item.getSelectedValues().isEmpty() ? item.getDefaultValue() : appendAllValuesInOneString(item));
        viewHolder.title.setText(item.getCaption());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private String appendAllValuesInOneString(ValueChooserListItem item){
        StringBuffer buffer = new StringBuffer();
        for (String value : item.getSelectedValues()){
            buffer.append(value);
            buffer.append(StringConstants.SPACE);
        }
        return buffer.toString();
    }

    class ViewHolder extends BaseViewHolder{
        @Bind(R.id.selectedValue) TextView selectedValue;
        @Bind(R.id.title) TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
