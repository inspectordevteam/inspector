package ru.sergeymaslov.inspector.Interface.DataManagers;



import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.raizlabs.android.dbflow.sql.language.Select;

import java.io.File;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ru.sergeymaslov.inspector.BaseClasses.IName;
import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.Car;
import ru.sergeymaslov.inspector.Driver;
import ru.sergeymaslov.inspector.Event;
import ru.sergeymaslov.inspector.Exceptions.EventSaveNoDataException;
import ru.sergeymaslov.inspector.FileTools;
import ru.sergeymaslov.inspector.Interface.Lists.PhotoListItem;
import ru.sergeymaslov.inspector.Interface.Lists.ValueChooserListItem;
import ru.sergeymaslov.inspector.Interface.Lists.ViolationCommentDialogItem;
import ru.sergeymaslov.inspector.Presenter.NewEventPresenter;
import ru.sergeymaslov.inspector.R;
import ru.sergeymaslov.inspector.StringConstants;
import ru.sergeymaslov.inspector.Syncronization.SyncStatusIds;
import ru.sergeymaslov.inspector.Tools.BitmapTools;
import ru.sergeymaslov.inspector.Tools.UniqueIdGenerator;
import ru.sergeymaslov.inspector.Violation;
import ru.sergeymaslov.inspector.ViolationInfo;

/**
 * Created by Администратор on 28.02.2016.
 */
public class NewEventDataManager {
    private final String TAG = "NewEventManager";
    private static NewEventDataManager mInstance;

    private HashSet<String> mPhotoFilesNames = new HashSet<>();

    public static final int POSITION_DRIVER = 0;
    public static final int POSITION_CAR = 1;
    public static final int POSITION_VIOLATION = 2;
    public static final int POSITION_PHOTO = 3;
    public static final int POSITION_FEESUM = 4;
    public static final int POSITION_VIOLATION_COMMENT = 5;

    //PHOTOS
    private List<PhotoListItem> mPhotoListItems = new ArrayList<>();
    private String newPhotoPath;
    private String newPhotoName;

    private ArrayList<ValueChooserListItem> mItems = new ArrayList<>();
    private Driver selectedDriver = null;
    private Car selectedCar = null;
    private ArrayList<ViolationInfo> selectedViolations;
    private HashMap<ViolationInfo, String> violationComments;

    private NewEventPresenter mNewEventPresenter;

    private String selectedFeeSum;

    public static NewEventDataManager getInstance(){
        if (mInstance == null){
            mInstance = new NewEventDataManager();
        }
        return mInstance;
    }
    private NewEventDataManager(){
        super();
        init();
    }

    public void init(){
        selectedViolations = new ArrayList<>();
        violationComments = new HashMap<>();
        initPhotoListItems();
    }
    public void initPhotoListItems(){
        final PhotoListItem addPhoto = new PhotoListItem(getDefaultItemBackground(), getString(R.string.photo_add),
                StringConstants.EMPTY, StringConstants.EMPTY);
        mPhotoListItems.add(addPhoto);
    }
    public void setNewEventPresenter(NewEventPresenter presenter){
        mNewEventPresenter = presenter;
    }
    public void setPhotoFilesNames(ArrayList<String> names){
        mPhotoFilesNames.addAll(names);
    }
    public Bitmap generateBitmap(int width, int height){
        if (newPhotoPath == null){
            return null;
        }
        String path = new File(newPhotoPath).getAbsolutePath();
        Bitmap bitmap = BitmapTools.decodeFromFile(path, width, height);
        return bitmap;
    }
    public ArrayList<String> getPhotosNames(){
        ArrayList<String> files = new ArrayList<>();
        for (PhotoListItem item : getPhotoListItems()){
            if (!item.getFileName().equals(StringConstants.EMPTY)){
                files.add(item.getFileName());
            }
        }
        return files;
    }

    public List<PhotoListItem> getPhotoListItems(){
        return mPhotoListItems;
    }

    private String getString(int resId){
        return InspectorApplication.getInstance().getString(resId);
    }

    private Bitmap getDefaultItemBackground(){
        return BitmapFactory.decodeResource(InspectorApplication.getInstance().getResources(),
                R.drawable.add_photo);
    }
    public void setNewPhotoName(String newPhotoName) {
        this.newPhotoName = newPhotoName;
    }
    public void saveNewPhoto(int width, int height){
        PhotoListItem newPhoto = new PhotoListItem(generateBitmap(width, height), getString(R.string.new_photo),
                newPhotoPath, newPhotoName);
        mPhotoListItems.add(newPhoto);
    }
    public void setNewPhotoPath(String newPhotoPath) {
        this.newPhotoPath = newPhotoPath;
    }
    public List<Driver> getDrivers(){
        List<Driver> drivers = new Select().from(Driver.class).queryList();
        return drivers;
    }

    public List<Car> getCars(){
        List<Car> cars = new Select().from(Car.class).queryList();
        return cars;
    }
    //move to data provider
    public List<ViolationInfo> getViolationInfos(){
        List<ViolationInfo> violations = new Select().from(ViolationInfo.class).queryList();
        return violations;
    }

    public CharSequence[] getDriversCharArray(){
        return toCharSequenceArray(getObjectNames(getDrivers()));
    }

    public CharSequence[] getCarsCharArray(){
        return toCharSequenceArray(getObjectNames(getCars()));
    }

    public CharSequence[] getViolationInfosCharArray(){
        return toCharSequenceArray(getObjectNames(getViolationInfos()));
    }

    public HashMap<ViolationInfo, String> getViolationComments(){
        for (ViolationInfo violationInfo : selectedViolations){
            if (!violationComments.containsKey(violationInfo)) {
                violationComments.put(violationInfo, null);
            }
        }
        return violationComments;
    }

    public void saveCommentForViolation(ViolationInfo info, String comment){
        violationComments.put(info, comment);
    }

    public ViolationCommentDialogItem getViolationCommentDialogItem(int position){
        ViolationInfo info = getViolationInfos().get(position);
        String comment = getViolationComments().get(info);
        ViolationCommentDialogItem item = new ViolationCommentDialogItem(info.violationText, comment);
        return item;
    }

    public void setSelectedDriver(Driver selectedDriver) {
        this.selectedDriver = selectedDriver;
    }

    public void setSelectedCar(Car selectedCar) {
        this.selectedCar = selectedCar;
    }

    public void addToSelectedViolations(ViolationInfo selectedViolation) {
        selectedViolations.add(selectedViolation);
    }

    public void removeFromSelectedViolations(ViolationInfo unselectedViolation){
        selectedViolations.remove(unselectedViolation);
        removeFromComments(unselectedViolation);
    }

    public boolean[] getCheckedViolations(){
        ArrayList<ViolationInfo> infos = (ArrayList<ViolationInfo>) getViolationInfos();
        boolean[] checkedItems = new boolean[infos.size()];
        for (int i = 0; i < infos.size(); i++){
            ViolationInfo info = infos.get(i);
            checkedItems[i] = selectedViolations.contains(info);
        }
        return checkedItems;
    }

    public void onNewDriverAdded(Driver driver){
        selectedDriver = driver;
        mNewEventPresenter.refreshView();
    }


    private void removeFromComments(ViolationInfo keyToRemove){
        Iterator<Map.Entry<ViolationInfo, String>> mapIterator = violationComments.entrySet().iterator();
        while(mapIterator.hasNext()){
            Map.Entry<ViolationInfo, String> entry = mapIterator.next();
            if (entry.getKey().equals(keyToRemove)){
                mapIterator.remove();
            }
        }
    }

    public void setSelectedFeeSum(String selectedFeeSum) {
        this.selectedFeeSum = selectedFeeSum;
    }

    public String getSelectedDriver() {
        return selectedDriver == null ? InspectorApplication.getInstance().getString(R.string.default_value) : selectedDriver.getObjectName();
    }

    public String getSelectedCar() {
        return selectedCar == null ? InspectorApplication.getInstance().getString(R.string.default_value) : selectedCar.getObjectName();
    }

    public String getSelectedViolation() {
        return selectedViolations.isEmpty() ? InspectorApplication.getInstance().getString(R.string.default_value) : generateSelectedViolationsArray();
    }

    private String generateSelectedViolationsArray(){
        StringBuffer buffer = new StringBuffer();
        for (ViolationInfo info : selectedViolations){
            buffer.append(info.getObjectName());
            buffer.append(StringConstants.SPACE);
        }
        return buffer.toString();
    }

    public String getSelectedFeeSum() {
        return selectedFeeSum == null ? "0" : selectedFeeSum;
    }

    public void saveNewViolation(String violation){
        ViolationInfo violationInfo = new ViolationInfo();
        violationInfo.violationText = violation;
        violationInfo.setSyncStatus(SyncStatusIds.STATUS_NEW);
        violationInfo.save();
        addToSelectedViolations(violationInfo);
    }

    public void saveNewDriver(Driver driver){
        driver.save();
        selectedDriver = driver;
    }

    public void saveEvent() throws EventSaveNoDataException{
        checkSelectedValuesNotEmpty();
        Event event = new Event();
        event.eventId = UniqueIdGenerator.generateUniqueId();
        event.feeSum = selectedFeeSum;
        event.carVinCode = selectedCar.vinCode;
        event.eventDate =  new Date();
        event.driverId = selectedDriver.driverId;
        event.setSyncStatus(SyncStatusIds.STATUS_NEW);
        event.save();
        saveViolations(event.eventId);
        saveEventFiles(FileTools.generateEventFolderName(event.eventId, selectedDriver.getObjectName()));
    }

    public void saveEventFiles(String folderName){
        File newEventDirectory = FileTools.createEventDirectory(folderName);
        if (newEventDirectory != null){
            //move all photos into this directory;
            ArrayList<String> files = getPhotosNames();
            FileTools.moveFilesFromOneDirectoryToAnother(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator
                    + FileTools.MAIN_DIRECTORY_NAME, files, newEventDirectory);
        }
        finish();
    }

    private void saveViolations(long eventId){
        if (violationComments.isEmpty()){
            violationComments = getViolationComments();
        }
        for (Map.Entry<ViolationInfo, String> entry : violationComments.entrySet()){
            Violation violation = new Violation();
            violation.violationText = entry.getKey().getViolationText();
            violation.comment = entry.getValue() != null ? entry.getValue() : StringConstants.EMPTY;
            violation.eventId = eventId;
            violation.save();
        }
    }

    public void finish(){
        mInstance = null;
    }

    public int getPhotosCount(){
        return mPhotoListItems.size() - 1;
    }

    private void checkSelectedValuesNotEmpty() throws EventSaveNoDataException {
        if (selectedCar == null ||
                selectedDriver == null ||
                selectedViolations.isEmpty()){
            throw  new EventSaveNoDataException(InspectorApplication.getInstance().getString(R.string.data_empty_exception));
        }
    }

    private ArrayList<String> getObjectNames(List<? extends IName> values){
        ArrayList<String> finalList = new ArrayList<>(values.size());
        for (IName name : values){
            finalList.add(name.getObjectName());
        }
        return finalList;
    }

    private CharSequence[] toCharSequenceArray(ArrayList<String> values) {
        CharSequence[] array = new CharSequence[values.size()];
        for (int i = 0; i < values.size(); i++){
            array[i] = values.get(i);
        }
        return array;
    }
    public ArrayList<ValueChooserListItem> getItems(){
        return mItems;
    }
    public void setItems(ArrayList<ValueChooserListItem> items){
        mItems = items;
    }
}
