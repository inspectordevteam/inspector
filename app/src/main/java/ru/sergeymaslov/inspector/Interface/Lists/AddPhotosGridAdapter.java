package ru.sergeymaslov.inspector.Interface.Lists;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ru.sergeymaslov.inspector.Interface.DataManagers.NewEventDataManager;
import ru.sergeymaslov.inspector.R;

/**
 * Created by Администратор on 05.03.2016.
 */
public class AddPhotosGridAdapter extends BaseAdapter {
    private Context mContext;
    private NewEventDataManager mDataManager;

    public AddPhotosGridAdapter(Context context, NewEventDataManager dataManager){
        mContext = context;
        mDataManager = dataManager;
    }
    @Override
    public int getCount() {
        return mDataManager.getPhotoListItems().size();
    }

    @Override
    public Object getItem(int position) {
        return mDataManager.getPhotoListItems().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.photo_list_item_layout, null);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            holder.photoLabel = (TextView) convertView.findViewById(R.id.photoLabel);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        PhotoListItem item = (PhotoListItem)getItem(position);
        holder.photoLabel.setText(item.getDescription());
        if (item.getImage() != null) {
            holder.imageView.setImageBitmap(item.getImage());
        }
        return convertView;
    }

    class ViewHolder{
        ImageView imageView;
        TextView photoLabel;
    }
}
