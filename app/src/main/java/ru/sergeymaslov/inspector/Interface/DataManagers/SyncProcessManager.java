package ru.sergeymaslov.inspector.Interface.DataManagers;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.Car;
import ru.sergeymaslov.inspector.Driver;
import ru.sergeymaslov.inspector.Driver_Table;
import ru.sergeymaslov.inspector.Event;
import ru.sergeymaslov.inspector.Event_Table;
import ru.sergeymaslov.inspector.FileTools;
import ru.sergeymaslov.inspector.Interface.Lists.ValueChooserListItem;
import ru.sergeymaslov.inspector.LastSyncDate;
import ru.sergeymaslov.inspector.LastSyncDate_Table;
import ru.sergeymaslov.inspector.Model.DatabaseDataProvider;
import ru.sergeymaslov.inspector.StringConstants;
import ru.sergeymaslov.inspector.Syncronization.ISyncResultListener;
import ru.sergeymaslov.inspector.Syncronization.SyncStatusIds;
import ru.sergeymaslov.inspector.Syncronization.SynchronizationTask;
import ru.sergeymaslov.inspector.Violation;

/**
 * Created by Администратор on 14.03.2016.
 */
public class SyncProcessManager {
    private static SyncProcessManager mInstance;
    private final String TAG = "SyncManager";

    private String defaultIP = "0.0.0.0";
    private int defaultPort = 12345;

    public static final int POSITION_IP = 0;
    public static final int POSITION_PORT = 1;

    public static final String KEY_DRIVER = "driver";
    public static final String KEY_VIOLATION = "violation";

    private final String KEY_PORT = "sync_port";
    private final String KEY_IP = "sync_ip";

    private String[] photoFileTypes = new String[]{".jpg", ".png"};
    private ArrayList<ValueChooserListItem> mItems = new ArrayList<>();
    private ArrayList<Car> mCars = new ArrayList<>();

    private DatabaseDataProvider mDatabaseDataProvider;


    private SyncProcessManager(){
        mDatabaseDataProvider = new DatabaseDataProvider();
    }

    public static SyncProcessManager getInstance(){
        if (mInstance == null){
            mInstance = new SyncProcessManager();
        }
        return mInstance;
    }

    public ArrayList<Event> getEvents(){
        ArrayList<Event> events = (ArrayList<Event>) new Select().from(Event.class).where
                (Condition.column(Event_Table.syncStatus.getNameAlias()).isNot(SyncStatusIds.STATUS_SENT)).queryList();
        return events;
    }

    public ArrayList<Violation> getAllViolations(){
        ArrayList<Violation> resultList = new ArrayList<>();
        for (Event event : getEvents()) {
            ArrayList<Violation> eventViolations = (ArrayList<Violation>) event.getViolations();
            resultList.addAll(eventViolations);
        }
        return resultList;
    }

    public ArrayList<Driver> getUnsyncedDrivers(){
        ArrayList<Driver> drivers  = (ArrayList<Driver>)new Select().from(Driver.class).where(
                Condition.column(Driver_Table.syncStatus.getNameAlias()).isNot(SyncStatusIds.STATUS_SENT)).queryList();
        return drivers;
    }

    public void deleteDrivers(){
        ArrayList<Driver> drivers = getAllDrivers();
        for (Driver driver : drivers){
            driver.delete();
        }
    }

    public ArrayList<Car> getCars(){
        if (mCars.isEmpty()){
            mCars = (ArrayList<Car>) new Select().from(Car.class).queryList();
        }
        return mCars;
    }

    public File[] getEventPhotos(Event event){
        File eventFolder = FileTools.getEventFolder(FileTools.generateEventFolderName(event.eventId, event.getDriver().getObjectName()));
        File[] eventPhotos = null;
        if (eventFolder != null){
            FilenameFilter filter = (dir, filename) -> {
                for (String fileType : photoFileTypes) {
                    if (filename.endsWith(fileType)) {
                        return true;
                    }
                }
                return false;
            };
            eventPhotos = eventFolder.listFiles(filter);
        }
        Log.d(TAG, eventPhotos.toString());
        return eventPhotos;
    }

    public void startSync(ProgressDialog dialog, ISyncResultListener syncResultListener) throws IOException {
        if (!getSelectedIP().equals(StringConstants.EMPTY) && getSelectedPort() != 0){
            SynchronizationTask syncTask = new SynchronizationTask(dialog, this, syncResultListener);
            syncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        } else {
            //TODO show informing dialog. Not a big chance this will ever happen though.
        }
    }

    public String getSelectedIP() {
        String savedIP = InspectorApplication.getInstance().getStringFromSharedPreferences(KEY_IP);
        return !savedIP.equals(StringConstants.EMPTY) ? savedIP : defaultIP;
    }

    public int getSelectedPort() {
        Integer savedPort = InspectorApplication.getInstance().getIntegerFromSharedPreferences(KEY_PORT);
        return savedPort != 0 ? savedPort : defaultPort;
    }

    public void savePort(int port){
        InspectorApplication.getInstance().saveIntegerToSharedPreferences(KEY_PORT, port);
    }

    public void saveIP(String ip){
        InspectorApplication.getInstance().saveStringToSharedPreferences(KEY_IP, ip);
    }

    public int getObjectsToSyncCount() {
        int eventsCount = getEvents().size();
        //other objects are to be added later
        return eventsCount;
    }
    public ArrayList<ValueChooserListItem> getItems(){
        return mItems;
    }
    public void setItems(ArrayList<ValueChooserListItem> items){
        mItems = items;
    }

    public Date getLastSyncDateFor(String key) {
        return mDatabaseDataProvider.getLastSyncDateFor(key);
    }

    private ArrayList<Driver> getAllDrivers(){
        ArrayList<Driver> drivers = (ArrayList<Driver>)new Select().from(Driver.class).queryList();
        if (drivers != null){
            return drivers;
        } else {
            return new ArrayList<>();
        }
    }


}
