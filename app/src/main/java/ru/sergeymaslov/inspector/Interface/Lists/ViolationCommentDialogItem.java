package ru.sergeymaslov.inspector.Interface.Lists;

/**
 * Created by Администратор on 15.03.2016.
 */
public class ViolationCommentDialogItem {
    private String violationName;
    private String violationComment;

    public ViolationCommentDialogItem(String violationName, String violationComment) {
        this.violationName = violationName;
        this.violationComment = violationComment;
    }

    public String getViolationName() {
        return violationName;
    }

    public String getViolationComment() {
        return violationComment;
    }
}
