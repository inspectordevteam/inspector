package ru.sergeymaslov.inspector.Interface;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.jakewharton.rxbinding.support.v7.widget.RxSearchView;
import java.util.ArrayList;
import java.util.List;
import ru.sergeymaslov.inspector.Presenter.EventHistoryPresenter;
import ru.sergeymaslov.inspector.View.IEventHistoryView;
import ru.sergeymaslov.inspector.BaseClasses.BaseActivity;
import ru.sergeymaslov.inspector.Dialogs.Dialog;
import ru.sergeymaslov.inspector.Dialogs.DialogManager;
import ru.sergeymaslov.inspector.Interface.Lists.EventListAdapter;
import ru.sergeymaslov.inspector.Interface.Lists.EventListItem;
import ru.sergeymaslov.inspector.Interface.Lists.ViolationCommentAdapter;
import ru.sergeymaslov.inspector.R;
import ru.sergeymaslov.inspector.StringConstants;
import ru.sergeymaslov.inspector.Violation;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;


/**
 * Created by Sergey Maslov on 28.02.2016.
 */
public class EventHistoryActivity extends BaseActivity implements MediaScannerConnection.MediaScannerConnectionClient, IEventHistoryView {
    private final static String TAG = "EventHistoryA";
    private View.OnClickListener onItemClickListener;

    private final int POSITION_VIEW_PHOTOS = 0;
    private final int POSITION_VIEW_COMMENTS = 1;
    private final int POSITION_DELETE_EVENT = 2;

    private String mScanPath;
    private MediaScannerConnection mediaScannerConnection;
    private EventHistoryPresenter mPresenter;
    private EventListAdapter mAdapter;

    private Subscription mSearchViewEventSubscription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initListeners();
        mAdapter = new EventListAdapter(this);
        setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(onItemClickListener);
        mPresenter = new EventHistoryPresenter();
        mPresenter.bindActivity(this);
        mPresenter.loadItems();
    }

    @Override
    protected int getViewId() {
        return R.layout.event_history_activity;
    }

    private void initListeners(){
        onItemClickListener = v -> {
            int position = getRecyclerView().getChildAdapterPosition(v);
            mPresenter.selectEvent(position);
            DialogManager.listDialog(null, Dialog.DIALOG_SINGLECHOICE, new CharSequence[]{getString(R.string.view_photos), getString(R.string.view_violation_comments),
                getString(R.string.delete_event)}, (dialog, which) -> {
                switch (which){
                    case POSITION_VIEW_PHOTOS:{
                        scanMedia(position);
                        break;
                    }
                    case POSITION_VIEW_COMMENTS:{
                        ViolationCommentAdapter adapter = new ViolationCommentAdapter(EventHistoryActivity.this);
                        adapter.setItems((ArrayList<Violation>) mPresenter.getEvents().get(position).getViolations());
                        DialogManager.customListDialogWithAdapter(getString(R.string.violation_comment_item_caption), Dialog.DIALOG_CUSTOM_LIST, R.layout.violation_comment_dialog_layout,
                                adapter).show(getFragmentManager(), DIALOG);
                        break;
                    }
                    case POSITION_DELETE_EVENT:{
                        DialogManager.confirmDialog(getString(R.string.delete_event_caption),
                                getString(R.string.confirm_delete_event),
                                Dialog.DIALOG_CONFIRM,
                                (dialog1, which1) -> {
                                    mPresenter.deleteEvent(mPresenter.getEvents().get(position));
                                },
                                ((dialog2, which2) -> {
                                    dialog2.dismiss();
                                })).show(getFragmentManager(), DIALOG);
                    }
                }
            }).show(getFragmentManager(), DIALOG);
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.bindActivity(this);
    }

    @Override
    protected void onPause() {
        mSearchViewEventSubscription.unsubscribe();
        mPresenter.unbindActivity();
        super.onPause();
    }

    @Override
    public void onMediaScannerConnected() {
        Log.d(TAG, "media scanner connected");
        if (!mScanPath.equals(StringConstants.EMPTY)) {
            mediaScannerConnection.scanFile(mScanPath, StringConstants.FILE_TYPE_EVERY_FILE);
        }
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {
        if (uri != null){
            Intent viewPicsIntent = new Intent(Intent.ACTION_VIEW);
            viewPicsIntent.setDataAndType(Uri.parse("file://" + path), "resource/folder");
            startActivity(viewPicsIntent);
        }
        mediaScannerConnection.disconnect();
        mediaScannerConnection = null;
    }

    private void scanMedia(int position){
        mScanPath = mPresenter.initScanning(position);
        if (mediaScannerConnection != null){
            mediaScannerConnection.disconnect();
        }
        mediaScannerConnection = new MediaScannerConnection(EventHistoryActivity.this, EventHistoryActivity.this);
        mediaScannerConnection.connect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_event_history, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        initSearchViewTextListener(searchView);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        MenuItemCompat.OnActionExpandListener expandListener = new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                mPresenter.onSearchViewClose();
                return true;
            }
        };
        MenuItemCompat.setOnActionExpandListener(menu.findItem(R.id.search), expandListener);
        return true;
    }

    private void initSearchViewTextListener(SearchView view){
        Action1<CharSequence> onNextAction = sequence -> mPresenter.filterItemsOnSearchQueryTextChange(sequence);
        mSearchViewEventSubscription = RxSearchView.queryTextChanges(view).filter(charSequence -> !TextUtils.isEmpty(charSequence))
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNextAction);
    }

    @Override
    public void onItemsLoaded(List<EventListItem> list) {
        mAdapter.setItems(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void refreshAdapter() {
        mAdapter.notifyDataSetChanged();
    }
}
