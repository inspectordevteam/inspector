package ru.sergeymaslov.inspector.Interface.Lists;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by Администратор on 09.04.2016.
 */
public abstract class BaseRecyclerViewAdapter<T>  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    protected Context mContext;
    private ArrayList<T> mItems;
    private View.OnClickListener mItemClickListener;

    public BaseRecyclerViewAdapter(Context context){
        mContext = context;
    }
    public void setItems(ArrayList<T> items){
        mItems = items;
    }
    public Object getItem(int position) {
        return mItems.get(position);
    }
    @Override
    public int getItemCount() {
        return mItems.size();
    }
    public void setOnItemClickListener(View.OnClickListener itemClickListener){
        mItemClickListener = itemClickListener;
    }

    class BaseViewHolder extends RecyclerView.ViewHolder{
        public BaseViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(mItemClickListener);
            ButterKnife.bind(this, itemView);
        }
    }
}
