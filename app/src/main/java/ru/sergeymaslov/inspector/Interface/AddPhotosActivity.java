package ru.sergeymaslov.inspector.Interface;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.GridView;

import java.io.File;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.sergeymaslov.inspector.FileTools;
import ru.sergeymaslov.inspector.Interface.DataManagers.NewEventDataManager;
import ru.sergeymaslov.inspector.Interface.Lists.AddPhotosGridAdapter;
import ru.sergeymaslov.inspector.Interface.Lists.PhotoListItem;
import ru.sergeymaslov.inspector.R;
import ru.sergeymaslov.inspector.StringConstants;

/**
 * Created by Администратор on 05.03.2016.
 */
public class AddPhotosActivity extends Activity {
    private NewEventDataManager mDataManager;
    private AddPhotosGridAdapter mAdapter;
    @Bind(R.id.gridView)protected GridView mGrid;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int MY_PERMISSIONS_REQUEST_STORAGE = 2;

    private static final int photoThumbnailWidth = 96;
    private static final int photoThumbnailHeight = 96;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.add_photos_activity_layout);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mDataManager = NewEventDataManager.getInstance();
        mAdapter = new AddPhotosGridAdapter(this, mDataManager);
        mGrid.setAdapter(mAdapter);
        mGrid.setOnItemClickListener((parent, view, position, id) -> {
            if (position == 0) {
                onTakePictureClick();
            } else {
                openGallery(mDataManager.getPhotoListItems().get(position));
            }
        });
        checkPermissions();
    }

    private void openGallery(PhotoListItem item){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file://" + item.getFilePath()), "image/*");
        startActivity(intent);
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheckWrite != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_STORAGE);
            }
        }
    }

    private void onTakePictureClick() {
        File photoFile = null;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        Intent chooser = Intent.createChooser(takePictureIntent, "Choose an app to take a photo");

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            try {
                photoFile = FileTools.createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
        mDataManager.setNewPhotoName(photoFile.getName());
        mDataManager.setNewPhotoPath(photoFile != null ? photoFile.getAbsolutePath() : StringConstants.EMPTY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_TAKE_PHOTO: {
                if (resultCode == Activity.RESULT_OK) {
                    mDataManager.saveNewPhoto(photoThumbnailWidth, photoThumbnailHeight);
                    mAdapter.notifyDataSetChanged();
                }
                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        //TODO handle situation when no new photos were taken. In this case we need to setResult(RESULT_CANCELED)
        Intent intent = new Intent();
        intent.putStringArrayListExtra(NewEventActivity.KEY_PHOTO_FILENAME, mDataManager.getPhotosNames());
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }
}
