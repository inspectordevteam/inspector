package ru.sergeymaslov.inspector.Interface.Lists;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;

/**
 * Created by Администратор on 05.03.2016.
 */
public class PhotoListItem{
    private Bitmap mImage;
    private String mDescription;
    private String mFilePath;
    private String mFileName;

    public PhotoListItem(Bitmap image, String description, String filePath, String fileName){
        mDescription = description;
        mImage = image;
        mFilePath = filePath;
        mFileName = fileName;
    }

    public Bitmap getImage() {
        return mImage;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public String getFileName() {
        return mFileName;
    }
}
