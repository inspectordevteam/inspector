package ru.sergeymaslov.inspector.Interface.Lists;

/**
 * Created by Администратор on 28.02.2016.
 */
public class ListItemField {
    private String value;
    private boolean isHighlighted;

    public ListItemField(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isHighlighted() {
        return isHighlighted;
    }

    public void setIsHighlighted(boolean isHighlighted) {
        this.isHighlighted = isHighlighted;
    }
}
