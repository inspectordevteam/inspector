package ru.sergeymaslov.inspector.Interface.DataManagers;

import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.sergeymaslov.inspector.Event;
import ru.sergeymaslov.inspector.FileTools;
import ru.sergeymaslov.inspector.Interface.Lists.EventListItem;
import ru.sergeymaslov.inspector.StringConstants;

/**
 * Created by Администратор on 28.02.2016.
 */
public class EventHistoryDataManager{
    private final static String TAG = "EventHistoryDM";
    private List<Event> mEvents;
    private List<EventListItem> mEventItems;
    private EventListItem selectedEvent;
    private static EventHistoryDataManager mInstance;

    private EventHistoryDataManager() {
        mEvents = new ArrayList<>();
        mEventItems = new ArrayList<>();
    }

    public static EventHistoryDataManager getInstance() {
        if (mInstance == null) {
            mInstance = new EventHistoryDataManager();

        }
        return mInstance;
    }

    public String initScanning(int position) {
        String scanPath = StringConstants.EMPTY;
        Event event = getEvent(position);
        File eventFolder = FileTools.getEventFolder(FileTools.generateEventFolderName(event.eventId, event.getDriver().getObjectName()));
        if (eventFolder != null) {
            scanPath = eventFolder.getAbsolutePath();
        }
        Log.d(TAG, "Scan path is " + scanPath);
        return scanPath;
    }
    public Event getEvent(int position) {
        return mEvents.get(position);
    }

    public void selectEvent(int position) {
        selectedEvent = mEventItems.get(position);
    }

    public EventListItem getSelectedEvent() {
        return selectedEvent;
    }
}
