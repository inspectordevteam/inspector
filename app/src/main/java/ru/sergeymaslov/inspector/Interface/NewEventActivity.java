package ru.sergeymaslov.inspector.Interface;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import butterknife.Bind;
import ru.sergeymaslov.inspector.BaseClasses.BaseActivity;
import ru.sergeymaslov.inspector.Dialogs.DialogManager;
import ru.sergeymaslov.inspector.Dialogs.Dialog;
import ru.sergeymaslov.inspector.Exceptions.EventSaveNoDataException;
import ru.sergeymaslov.inspector.Interface.DataManagers.NewEventDataManager;
import ru.sergeymaslov.inspector.Interface.Lists.ValueChooserListAdapter;
import ru.sergeymaslov.inspector.Interface.Lists.ValueChooserListItem;
import ru.sergeymaslov.inspector.Presenter.NewEventPresenter;
import ru.sergeymaslov.inspector.R;
import ru.sergeymaslov.inspector.View.INewEventView;
import ru.sergeymaslov.inspector.ViolationInfo;


public class NewEventActivity extends BaseActivity implements INewEventView {
    @Bind(R.id.fab)
    protected FloatingActionButton fab;
    private NewEventPresenter mPresenter;
    private ValueChooserListAdapter adapter;

    public static final int REQUEST_PHOTOS = 100;
    public static final String KEY_PHOTO_FILENAME = "filename";

    private NewEventDataManager mDataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO eliminate usage of data manager in activity, move it to presenter
        mDataManager = NewEventDataManager.getInstance();
        mPresenter = new NewEventPresenter();
        adapter = new ValueChooserListAdapter(this);

        setAdapter(adapter);
        adapter.setOnItemClickListener(v -> {
            int position = getRecyclerView().getChildAdapterPosition(v);
            mPresenter.onItemClick(position);
        });
        initWidgets();
    }

    @Override
    protected int getViewId() {
        return R.layout.new_event_activity;
    }

    private void initWidgets() {
        fab.setBackgroundColor(Color.BLUE);
        fab.setOnClickListener(v -> {
            DialogInterface.OnClickListener positiveClick = (dialog, which) -> {
                try {
                    mPresenter.saveEvent();
                } catch (EventSaveNoDataException e) {
                    DialogManager.infoDialog(getString(R.string.error), e.getMessage(), Dialog.DIALOG_MESSAGE).show(getFragmentManager(), DIALOG);
                    return;
                }
                Intent intent = new Intent(NewEventActivity.this, LauncherActivity.class);
                startActivity(intent);
            };
            DialogInterface.OnClickListener negativeClick = (dialog, which) -> dialog.dismiss();

            DialogManager.confirmDialog(getString(R.string.confirm_action), getString(R.string.confirm_save_event), Dialog.DIALOG_CONFIRM,
                    positiveClick, negativeClick).show(getFragmentManager(), DIALOG);

        });
    }

    @Override
    protected void onResume() {
        mPresenter.bindActivity(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        mPresenter.unbindActivity();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        finish();
        mDataManager.finish();
    }

    @Override
    public NewEventDataManager getDataManager() {
        return null;
    }

    @Override
    public void onItemsRefresh(ArrayList<ValueChooserListItem> items) {
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showDialog(DialogFragment dialog) {
        dialog.show(getFragmentManager(), DIALOG);
    }

    @Override
    public void gotoPhotos() {
        Intent intent = new Intent(NewEventActivity.this, AddPhotosActivity.class);
        startActivityForResult(intent, REQUEST_PHOTOS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            mPresenter.updatePhotosInformation(data);
        }
    }

    @Override
    public void showViolationCommentsDialog() {
        DialogManager.customListDialog(getString(R.string.violation_comment_item_caption), Dialog.DIALOG_CUSTOM_VIEW, R.layout.violation_comment_dialog_layout,
                generateCustomViewForVComments()).show(getFragmentManager(), DIALOG);
    }

    private View generateCustomViewForVComments() {
        View parent = LayoutInflater.from(this).inflate(R.layout.vcomment_edit_dialog_parent, null);
        LinearLayout resultContainer = (LinearLayout) parent.findViewById(R.id.parentContainer);
        resultContainer.removeAllViews();
        for (final Map.Entry<ViolationInfo, String> entry : mPresenter.getViolationComments().entrySet()) {
            View childContainer = LayoutInflater.from(this).inflate(R.layout.violation_comment_dialog_item, null);
            final EditText editor = (EditText) childContainer.findViewById(R.id.violationValue);
            //TODO substitute this listener with something shorter (from rx I guess)
            editor.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    mDataManager.saveCommentForViolation(entry.getKey(), s.toString());
                }
            });
            if (entry.getValue() != null) {
                editor.setText(entry.getValue());
            }
            TextView violationName = (TextView) childContainer.findViewById(R.id.violationLabel);
            violationName.setText(entry.getKey().getViolationText());
            resultContainer.addView(childContainer);
        }
        return parent;
    }
}
