package ru.sergeymaslov.inspector.Interface;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import java.io.IOException;

import butterknife.Bind;
import ru.sergeymaslov.inspector.BaseClasses.BaseActivity;
import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.Dialogs.Dialog;
import ru.sergeymaslov.inspector.Dialogs.DialogManager;
import ru.sergeymaslov.inspector.Interface.DataManagers.SyncProcessManager;
import ru.sergeymaslov.inspector.Presenter.LaunchActivityPresenter;
import ru.sergeymaslov.inspector.R;
import ru.sergeymaslov.inspector.Syncronization.ISyncResultListener;
import ru.sergeymaslov.inspector.View.ILauncherView;

public class LauncherActivity extends BaseActivity implements ILauncherView {
    @Bind(R.id.commitViolation)protected RelativeLayout commitViolationButton;
    @Bind(R.id.inspection)protected RelativeLayout inspectionButton;
    @Bind(R.id.eventHistory)protected RelativeLayout eventHistoryButton;
    @Bind(R.id.container)protected CoordinatorLayout container;

    private View.OnClickListener commitViolationClickListener;
    private View.OnClickListener inspectionClickListener;
    private View.OnClickListener eventHistoryClickListener;

    private ISyncResultListener mSyncResultListener;

    private SyncProcessManager mSyncProcessManager;

    private LaunchActivityPresenter mPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new LaunchActivityPresenter();
        mSyncProcessManager = SyncProcessManager.getInstance();
        initListeners();
    }

    @Override
    protected void onResume() {
        mPresenter.bindActivity(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        mPresenter.unbindActivity();
        super.onDestroy();
    }


    @Override
    protected int getViewId() {
        return R.layout.activity_launcher;
    }

    private void initListeners(){
        mSyncResultListener = new ISyncResultListener() {
            @Override
            public void onSyncSuccess() {
                DialogManager.infoDialog(null, getString(R.string.sync_success_message), Dialog.DIALOG_MESSAGE).show(getFragmentManager(), DIALOG);
            }

            @Override
            public void onSyncFailed() {
                DialogManager.infoDialog(null, getString(R.string.sync_failed_message), Dialog.DIALOG_MESSAGE).show(getFragmentManager(), DIALOG);
            }
        };
        commitViolationClickListener = v -> {
            Intent intent = new Intent(LauncherActivity.this, NewEventActivity.class);
            startActivity(intent);
        };
        commitViolationButton.setOnClickListener(commitViolationClickListener);
        inspectionClickListener = v -> {

        };
        inspectionButton.setOnClickListener(inspectionClickListener);
        eventHistoryClickListener = v -> {
            Intent intent = new Intent(LauncherActivity.this, EventHistoryActivity.class);
            startActivity(intent);
        };
        eventHistoryButton.setOnClickListener(eventHistoryClickListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_launcher, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_tosdcard) {
            if (checkPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, MY_PERMISSIONS_REQUEST_STORAGE, Build.VERSION_CODES.M)){
                try {
                    InspectorApplication.getInstance().copyDBToSDCard();
                    onDbCopySuccess();
                } catch (IOException e) {
                    onDbCopyToSDCardFailed();
                    e.printStackTrace();
                }
            } else {
                mPresenter.setCurrentRequest(LaunchActivityPresenter.REQUEST_COPY_DB_TO_SD);
            }
            return true;
        } else if (id == R.id.action_tointernal){
            if (checkPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, MY_PERMISSIONS_REQUEST_STORAGE, Build.VERSION_CODES.M)) {
                try {
                    InspectorApplication.getInstance().copyDBFromSDCard();
                    onDbCopySuccess();
                } catch (IOException e) {
                    onDbCopyToInnerStorageFailed();
                    e.printStackTrace();
                }
            } else {
                mPresenter.setCurrentRequest(LaunchActivityPresenter.REQUEST_COPY_DB_FROM_SD);
            }
            return true;
        } else if (id == R.id.action_sync){
            try {
                mSyncProcessManager.startSync(createSyncProgressDialog(), mSyncResultListener);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDbCopyToSDCardFailed(){
        Snackbar.make(container, getString(R.string.db_copy_to_sd_failed), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onDbCopyToInnerStorageFailed(){
        Snackbar.make(container, getString(R.string.db_copy_to_inner_storage_failed), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onDbCopySuccess(){
        Snackbar.make(container, getString(R.string.db_copy_success), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(requestCode){
            case MY_PERMISSIONS_REQUEST_STORAGE:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    mPresenter.onWriteStoragePermissionGranted();
                } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED){
                    /*Snackbar.make(LauncherActivity.this.getRecyclerView(), "Необходимо предоставить приложению разрешение на выполнение данной операции", Snackbar.LENGTH_LONG).show();*/
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private ProgressDialog createSyncProgressDialog() {
        ProgressDialog progressDialog = new ProgressDialog(LauncherActivity.this);
        progressDialog.setTitle(getString(R.string.sync));
        progressDialog.setMessage(getString(R.string.preparing_sync));
        progressDialog.setMax(mSyncProcessManager.getObjectsToSyncCount());
        progressDialog.setProgress(0);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }
}
