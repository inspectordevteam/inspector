package ru.sergeymaslov.inspector.Interfaces;

/**
 * Created by Администратор on 25.03.2016.
 */
public interface Syncable {
    String getMessage();
    void onSyncFinished();
    String getPath();
}
