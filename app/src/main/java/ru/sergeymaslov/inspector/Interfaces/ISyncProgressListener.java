package ru.sergeymaslov.inspector.Interfaces;

import android.util.Pair;

/**
 * Created by Администратор on 25.03.2016.
 */
public interface ISyncProgressListener {
    void onProgressUpdate(Pair<Integer, Object> progress);
}
