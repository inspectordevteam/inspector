package ru.sergeymaslov.inspector.Tools;

import android.support.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Администратор on 15.05.2016.
 */
public class DateTools {
    public static String dateToString(Date date, @Nullable String format){
        SimpleDateFormat dateFormat;
        if (format != null) {
           dateFormat = new SimpleDateFormat(format);
        } else {
            dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        }
        return dateFormat.format(date);
    }
}
