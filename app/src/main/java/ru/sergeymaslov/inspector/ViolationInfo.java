package ru.sergeymaslov.inspector;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import ru.sergeymaslov.inspector.BaseClasses.IName;

/**
 * Created by Администратор on 29.02.2016.
 */
@Table(database = InspectorDatabase.class)
public class ViolationInfo extends BaseModel implements IName{
    @PrimaryKey
    public String violationText;
    @Column
    private int syncStatus;

    public int getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(int syncStatus) {
        this.syncStatus = syncStatus;
    }

    public ViolationInfo(){}

    public String getViolationText(){
        return violationText;
    }

    @Override
    public String getObjectName() {
        return violationText;
    }

    @Override
    public boolean equals(Object o) {
        ViolationInfo info = (ViolationInfo) o;
        return info.getViolationText().equals(this.violationText);
    }
}
