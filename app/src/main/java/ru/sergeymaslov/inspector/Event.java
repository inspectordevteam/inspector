package ru.sergeymaslov.inspector;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import ru.sergeymaslov.inspector.Annotations.Sync;
import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.Interfaces.Syncable;

import ru.sergeymaslov.inspector.Syncronization.SyncStatusIds;
import ru.sergeymaslov.inspector.Tools.DateTools;


@Table(database = InspectorDatabase.class)
public class Event extends BaseModel implements Serializable, Syncable {
    private static final long serialVersionUID = -9155842406504899729L;

    @Sync
    @PrimaryKey
    public long eventId;

    @Sync
    @Column
    public Date eventDate;

    @Sync
    @Column
    public String feeSum;

    @Sync
    @Column
    public long driverId;

    @Sync
    @Column
    public String carVinCode;

    @Column
    private int syncStatus;

    public int getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(int syncStatus) {
        this.syncStatus = syncStatus;
    }

    private List<Violation> violations;

    public Car car;

    public Driver driver;

    public Event(){
        violations = new ArrayList<>();
    }

    public Car getCar(){
        if (car == null){
            car = new Select().from(Car.class).where(Condition.column(Car_Table.vinCode.getNameAlias()).is(this.carVinCode)).querySingle();
        }
        return car;
    }

    public Driver getDriver(){
        if (driver == null){
            driver = new Select().from(Driver.class).where(Condition.column(Driver_Table.driverId.getNameAlias()).is(this.driverId)).querySingle();
        }
        return driver;
    }

    public List<Violation> getViolations(){
        violations = new Select().from(Violation.class).where(Condition.column(Violation_Table.eventId.getNameAlias()).is(this.eventId)).queryList();
        return violations;
    }

    public long getEventId() {
        return eventId;
    }

    public long getDriverId(){
        return driverId;
    }

    public String getEventDate() {
        return String.valueOf(eventDate);
    }

    public String getFeeSum() {
        return feeSum;
    }

    public String getCarVinCode() {
        return carVinCode;
    }

    @Override
    public String getMessage() {
        return InspectorApplication.getInstance().getString(R.string.sync_dialog_event_sync_started_message);
    }

    @Override
    public void onSyncFinished() {
        syncStatus = SyncStatusIds.STATUS_SENT;
        save();
    }

    @Override
    public String getPath() {
        return "save-events";
    }
}
