package ru.sergeymaslov.inspector.Annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Администратор on 25.03.2016.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Sync {
    String path() default "";
}
