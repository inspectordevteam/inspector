package ru.sergeymaslov.inspector;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

import ru.sergeymaslov.inspector.Annotations.Sync;
import ru.sergeymaslov.inspector.BaseClasses.IName;
import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.Interfaces.Syncable;

/**
 * Created by Администратор on 26.02.2016.
 */
@Table(database = InspectorDatabase.class)
public class Car extends BaseModel implements IName, Syncable{
    @Sync
    @PrimaryKey
    public String vinCode;

    @Sync
    @Column
    public String call;

    @Sync
    @Column
    public String carNumber;

    @Sync
    @Column
    public String model;

    List<Event> events;
    List<Driver> drivers;

    public Car(){}

    @Override
    public String getObjectName() {
        return call + StringConstants.SPACE + model + StringConstants.SPACE + carNumber;
    }

    public String getVinCode() {
        return vinCode;
    }

    public String getCall() {
        return call;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public String getModel() {
        return model;
    }

    @Override
    public String getMessage() {
        return InspectorApplication.getInstance().getString(R.string.cars);
    }

    @Override
    public void onSyncFinished() {

    }

    @Override
    public String getPath() {
        return null;
    }
}
