package ru.sergeymaslov.inspector.Presenter;

import android.content.DialogInterface;
import android.content.Intent;
import android.text.InputType;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import ru.sergeymaslov.inspector.Dialogs.AddDriverDialog;
import ru.sergeymaslov.inspector.Dialogs.Dialog;
import ru.sergeymaslov.inspector.Dialogs.DialogManager;
import ru.sergeymaslov.inspector.Exceptions.EventSaveNoDataException;
import ru.sergeymaslov.inspector.Interface.DataManagers.NewEventDataManager;
import ru.sergeymaslov.inspector.Interface.Lists.ValueChooserListItem;
import ru.sergeymaslov.inspector.Interface.NewEventActivity;
import ru.sergeymaslov.inspector.Interfaces.IEditTextClick;
import ru.sergeymaslov.inspector.R;
import ru.sergeymaslov.inspector.StringConstants;
import ru.sergeymaslov.inspector.View.INewEventView;
import ru.sergeymaslov.inspector.View.IView;
import ru.sergeymaslov.inspector.ViolationInfo;
import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

/**
 * Created by Администратор on 11.04.2016.
 */
public class NewEventPresenter extends BasePresenter {

    private WeakReference<INewEventView> mView;
    private PublishSubject mPublishSubject;
    private Subscription mSubscription;
    private NewEventDataManager mDataManager;

    private static final int ID_ADD_VIOLATION = 0;
    private static final int ID_CREATE_VIOLATION = 1;
    private static final int ID_ADD_DRIVER = 0;
    private static final int ID_CREATE_DRIVER = 1;



    @Override
    public void bindActivity(IView view) {
        mView = new WeakReference<>((INewEventView) view);
        mDataManager = NewEventDataManager.getInstance();
        mDataManager.setNewEventPresenter(this);
        mPublishSubject = PublishSubject.create();
        mSubscription = mPublishSubject.asObservable().subscribe((Action1) o -> {
            mView.get().onItemsRefresh(mDataManager.getItems());
        });
        initListItems();
    }

    @Override
    public void unbindActivity() {
        mSubscription.unsubscribe();
        mView = null;
    }

    private void initListItems(){
        ArrayList<ValueChooserListItem> items = new ArrayList<>();
        //DRIVERS
        final ValueChooserListItem driverList = new ValueChooserListItem(mView.get().getString(R.string.drivers), mDataManager.getSelectedDriver()) {
            @Override
            public void onClick() {
                DialogInterface.OnClickListener addDriverClickListener = (dialog, which) -> {
                    addToSelectedValues(mDataManager.getDrivers().get(which).getObjectName(), false);
                    mDataManager.setSelectedDriver(mDataManager.getDrivers().get(which));
                    mPublishSubject.onNext(null);
                };

                DialogInterface.OnClickListener onViolationActionChosenClickListener = (dialog, which) -> {
                    switch(which){
                        case ID_ADD_DRIVER: {
                            mView.get().showDialog(DialogManager.listDialog(mView.get().getString(R.string.drivers), Dialog.DIALOG_SINGLECHOICE,
                                    mDataManager.getDriversCharArray(), addDriverClickListener));
                            break;
                        }
                        case ID_CREATE_DRIVER: {
                            AddDriverDialog addDriverDialog = new AddDriverDialog();
                            addDriverDialog.setValueChooserListItem(this);
                            mView.get().showDialog(addDriverDialog);
                            break;
                        }
                    }
                };
                //FIRST LEVEL DIALOG; CHOOSE EITHER "CHOOSE VIOLATION" OR "CREATE NEW VIOLATION"
                mView.get().showDialog(DialogManager.listDialog(mView.get().getString(R.string.choose_violation_action), Dialog.DIALOG_SINGLECHOICE,
                        new CharSequence[]{mView.get().getString(R.string.choose_driver), mView.get().getString(R.string.add_driver)},
                        onViolationActionChosenClickListener));
            }
        };
        items.add(NewEventDataManager.POSITION_DRIVER, driverList);
        //CARS
        ValueChooserListItem carList = new ValueChooserListItem(mView.get().getString(R.string.cars), mDataManager.getSelectedCar()) {
            @Override
            public void onClick() {
                DialogInterface.OnClickListener clickListener = (dialog, which) -> {
                    addToSelectedValues(mDataManager.getCars().get(which).getObjectName(), false);
                    mDataManager.setSelectedCar(mDataManager.getCars().get(which));
                    mPublishSubject.onNext(null);
                };

                mView.get().showDialog(DialogManager.listDialog(mView.get().getString(R.string.cars), Dialog.DIALOG_SINGLECHOICE,
                        mDataManager.getCarsCharArray(), clickListener));
            }
        };
        items.add(NewEventDataManager.POSITION_CAR, carList);
        //VIOLATIONS
        ValueChooserListItem violationList = new ValueChooserListItem(mView.get().getString(R.string.add_violation), mDataManager.getSelectedViolation()) {
            @Override
            public void onClick() {
                final DialogInterface.OnMultiChoiceClickListener multiChoiceClickListener = (dialog, which, isChecked) -> {
                        if (isChecked){
                            addToSelectedValues(mDataManager.getViolationInfos().get(which).getObjectName(), true);
                            mDataManager.addToSelectedViolations(mDataManager.getViolationInfos().get(which));
                            mPublishSubject.onNext(null);
                        } else {
                            removeFromSelectedValues(mDataManager.getViolationInfos().get(which).getObjectName());
                            mDataManager.removeFromSelectedViolations(mDataManager.getViolationInfos().get(which));
                            mPublishSubject.onNext(null);
                        }
                };

                final IEditTextClick onViolationCreatedClickListener = data -> {
                    if (data != null && !data.equals(StringConstants.EMPTY)) {
                        addToSelectedValues(data, true);
                        mDataManager.saveNewViolation(data);
                        mPublishSubject.onNext(null);
                    }
                };

                DialogInterface.OnClickListener onViolationActionChosenClickListener = (dialog, which) -> {
                    switch(which){
                        case ID_ADD_VIOLATION: {
                            mView.get().showDialog(DialogManager.multiChoiceListDialog(mView.get().getString(R.string.add_violation), Dialog.DIALOG_MULTICHOICE,
                                    mDataManager.getViolationInfosCharArray(), mDataManager.getCheckedViolations(), multiChoiceClickListener));
                            break;
                        }
                        case ID_CREATE_VIOLATION: {
                            mView.get().showDialog(DialogManager.editTextDialog(mView.get().getString(R.string.enter_name_of_violation), StringConstants.EMPTY, Dialog.DIALOG_EDIT_TEXT,
                                    InputType.TYPE_CLASS_TEXT, onViolationCreatedClickListener));;
                            break;
                        }
                    }
                };
                //FIRST LEVEL DIALOG; CHOOSE EITHER "CHOOSE VIOLATION" OR "CREATE NEW VIOLATION"
                mView.get().showDialog(DialogManager.listDialog(mView.get().getString(R.string.choose_violation_action), Dialog.DIALOG_SINGLECHOICE,
                        new CharSequence[]{mView.get().getString(R.string.choose_violation), mView.get().getString(R.string.create_vilation)},
                        onViolationActionChosenClickListener));
            }
        };
        items.add(NewEventDataManager.POSITION_VIOLATION, violationList);
        //PHOTOS
        ValueChooserListItem addPhotos = new ValueChooserListItem(mView.get().getString(R.string.add_photos),
                String.format(mView.get().getString(R.string.photos_total), String.valueOf(mDataManager.getPhotosCount()))) {
            @Override
            public void onClick() {
                mView.get().gotoPhotos();
            }
        };
        addPhotos.addToSelectedValues(String.format(mView.get().getString(R.string.photos_total), String.valueOf(mDataManager.getPhotosCount())), false);
        items.add(NewEventDataManager.POSITION_PHOTO, addPhotos);
        //FEE SUM
        ValueChooserListItem feeSum = new ValueChooserListItem(mView.get().getString(R.string.fee_sum_caption), mDataManager.getSelectedFeeSum()) {
            @Override
            public void onClick() {
                mView.get().showDialog(DialogManager.editTextDialog(mView.get().getString(R.string.enter_fee_sum_dialog_caption),
                        mDataManager.getSelectedFeeSum().equals("0") ? StringConstants.EMPTY : mDataManager.getSelectedFeeSum(),
                        Dialog.DIALOG_EDIT_TEXT, InputType.TYPE_CLASS_NUMBER, data -> {
                            addToSelectedValues(data, false);
                            mDataManager.setSelectedFeeSum(data);
                            mPublishSubject.onNext(null);
                        }));
            }
        };
        feeSum.addToSelectedValues(mDataManager.getSelectedFeeSum(), false);
        items.add(NewEventDataManager.POSITION_FEESUM, feeSum);
        //VIOLATION COMMENT
        ValueChooserListItem violationComment = new ValueChooserListItem(mView.get().getString(R.string.violation_comment_item_caption), mView.get().getString(R.string.create_comment_caption)) {
            @Override
            public void onClick() {
                if (mDataManager.getViolationComments().isEmpty()){
                    mView.get().showDialog(DialogManager.infoDialog(mView.get().getString(R.string.error), mView.get().getString(R.string.violation_list_empty_error),
                            Dialog.DIALOG_MESSAGE));
                    return;
                }
                mView.get().showViolationCommentsDialog();
            }
        };
        items.add(NewEventDataManager.POSITION_VIOLATION_COMMENT, violationComment);
        mDataManager.setItems(items);
        mPublishSubject.onNext(null);
    }

    public void updatePhotosInformation(Intent data){
        mDataManager.setPhotoFilesNames(data.getStringArrayListExtra(NewEventActivity.KEY_PHOTO_FILENAME));
    }

    public void onItemClick(int position){
        mDataManager.getItems().get(position).onClick();
    }

    public void saveEvent() throws EventSaveNoDataException {
        mDataManager.saveEvent();
    }

    public HashMap<ViolationInfo, String> getViolationComments(){
        return mDataManager.getViolationComments();
    }

    public void refreshView(){
        mPublishSubject.onNext(null);
    }
}
