package ru.sergeymaslov.inspector.Presenter;

import ru.sergeymaslov.inspector.Model.DataManager;
import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.View.IView;

/**
 * Created by Администратор on 08.04.2016.
 */
public abstract class BasePresenter implements IPresenter{
    protected DataManager mDataManager;
    public BasePresenter(){
        mDataManager = InspectorApplication.getInstance().getDataManager();
    }
}
