package ru.sergeymaslov.inspector.Presenter;

/**
 * Created by Администратор on 08.04.2016.
 */
import android.util.Log;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ru.sergeymaslov.inspector.FileTools;
import ru.sergeymaslov.inspector.StringConstants;
import ru.sergeymaslov.inspector.Tools.DateTools;
import ru.sergeymaslov.inspector.View.IEventHistoryView;
import ru.sergeymaslov.inspector.View.IView;
import ru.sergeymaslov.inspector.Event;
import ru.sergeymaslov.inspector.Interface.Lists.EventListItem;
import ru.sergeymaslov.inspector.Violation;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class EventHistoryPresenter extends BasePresenter {
    private static final String TAG = "EvHistPres";
    private WeakReference<IEventHistoryView> mEventView;
    private Subscription mSubscription;
    private List<EventListItem> mEventItems = new ArrayList<>();
    private ArrayList<Event> mEvents = new ArrayList<>();

    private EventListItem mSelectedEvent;

    @Override
    public void bindActivity(IView view){
        if (mEventView == null) {
            mEventView = new WeakReference<>((IEventHistoryView) view);
        }
    }

    @Override
    public void unbindActivity() {
        mEventView = null;
        if (!mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }
    }

    public void deleteEvent(Event event){
        mEvents.remove(event);
        if (!mSubscription.isUnsubscribed()){
            mSubscription.unsubscribe();
        }
        mSubscription = mDataManager.getDataProvider().deleteObject(event)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber() {
                    @Override
                    public void onCompleted() {
                        if (mSubscription.isUnsubscribed()){
                            return;
                        }
                        mEventView.get().onItemsLoaded(createListItems(mEvents));
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {

                    }
                });
    }

    public void loadItems(){
        mSubscription = mDataManager.getDataProvider().getEvents()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Event>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Event> events) {
                        if (mSubscription.isUnsubscribed()){
                            return;
                        }
                        mEvents = (ArrayList<Event>) events;
                        mEventView.get().onItemsLoaded(createListItems(events));
                    }
                });
    }
    public ArrayList<Event> getEvents(){
        return mEvents;
    }

    private List<EventListItem> createListItems(List<Event> events){
        mEventItems.clear();
        ArrayList<EventListItem> items = new ArrayList<>();
        for (Event event : events){
            String eventDate = DateTools.dateToString(event.eventDate, null);
            EventListItem item = new EventListItem(eventDate,
                    event.getDriver().getObjectName(),
                    event.getCar().getObjectName());
            item.setViolations((ArrayList<Violation>) event.getViolations());
            items.add(item);
        }
        mEventItems.addAll(items);
        return mEventItems;
    }

    public void filterItemsOnSearchQueryTextChange(CharSequence charSequence){
        ArrayList<EventListItem> filteredList = new ArrayList<>();
        for (EventListItem item : mEventItems){
            if (item.hasFieldWithText(charSequence)){
                filteredList.add(item);
            }
        }
        mEventView.get().onItemsLoaded(filteredList);
    }

    public void onSearchViewClose(){
        mEventView.get().onItemsLoaded(mEventItems);
    }

    public String initScanning(int position) {
        String scanPath = StringConstants.EMPTY;
        Event event = getEvents().get(position);
        File eventFolder = FileTools.getEventFolder(FileTools.generateEventFolderName(event.eventId, event.getDriver().getObjectName()));
        if (eventFolder != null) {
            scanPath = eventFolder.getAbsolutePath();
        }
        Log.d(TAG, "Scan path is " + scanPath);
        return scanPath;
    }

    public void selectEvent(int position) {
        mSelectedEvent = mEventItems.get(position);
    }

    public EventListItem getSelectedEvent() {
        return mSelectedEvent;
    }

}
