package ru.sergeymaslov.inspector.Presenter;

import java.lang.ref.WeakReference;

import ru.sergeymaslov.inspector.Driver;
import ru.sergeymaslov.inspector.Interface.DataManagers.NewEventDataManager;
import ru.sergeymaslov.inspector.Interface.Lists.ValueChooserListItem;
import ru.sergeymaslov.inspector.Tools.UniqueIdGenerator;
import ru.sergeymaslov.inspector.View.IAddDriverDialogView;
import ru.sergeymaslov.inspector.View.IView;

/**
 * Created by Maslov on 21.04.2016.
 */
public class AddDriverDialogPresenter extends BasePresenter {
    private NewEventDataManager mDataManager;
    private WeakReference<IAddDriverDialogView> mView;
    @Override
    public void bindActivity(IView view) {
        mView = new WeakReference<>((IAddDriverDialogView) view);
        mDataManager = NewEventDataManager.getInstance();
    }

    @Override
    public void unbindActivity() {
        mView.clear();
    }

    public void saveDriver(String driverName, String driverSurname, String driverLastname, String driverLicenseNumber,
                           ValueChooserListItem valueChooserListItem){
        Driver driver = new Driver();
        driver.driverId = UniqueIdGenerator.generateUniqueId();
        driver.name = driverName;
        driver.surname = driverSurname;
        driver.lastName = driverLastname;
        driver.driverLicenseNumber = driverLicenseNumber;
        driver.save();
        valueChooserListItem.addToSelectedValues(driver.getObjectName(), false);
        mDataManager.onNewDriverAdded(driver);
    }
}
