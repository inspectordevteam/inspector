package ru.sergeymaslov.inspector.Presenter;

import java.io.IOException;
import java.lang.ref.WeakReference;

import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.View.ILauncherView;
import ru.sergeymaslov.inspector.View.IView;

/**
 * Created by Maslov on 20.04.2016.
 */
public class LaunchActivityPresenter extends BasePresenter {
    private WeakReference<ILauncherView> mView;
    public static final int REQUEST_COPY_DB_TO_SD = 1;
    public static final int REQUEST_COPY_DB_FROM_SD = 2;
    private int mCurrentRequest;
    @Override
    public void bindActivity(IView view) {
        mView = new WeakReference<>((ILauncherView) view);
    }

    @Override
    public void unbindActivity() {
        mView.clear();
    }

    public int getCurrentRequest() {
        return mCurrentRequest;
    }

    public void setCurrentRequest(int mCurrentRequest) {
        this.mCurrentRequest = mCurrentRequest;
    }

    public void onWriteStoragePermissionGranted(){
        switch(mCurrentRequest){
            case REQUEST_COPY_DB_TO_SD:{
                try {
                    InspectorApplication.getInstance().copyDBToSDCard();
                    mView.get().onDbCopySuccess();
                } catch (IOException e) {
                    mView.get().onDbCopyToSDCardFailed();
                    e.printStackTrace();
                }
                break;
            }
            case REQUEST_COPY_DB_FROM_SD:{
                try {
                    InspectorApplication.getInstance().copyDBFromSDCard();
                    mView.get().onDbCopySuccess();
                } catch (IOException e) {
                    mView.get().onDbCopyToInnerStorageFailed();
                    e.printStackTrace();
                }
                break;
            }
        }
        mCurrentRequest = 0;
    }
}
