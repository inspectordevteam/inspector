package ru.sergeymaslov.inspector.Presenter;

import ru.sergeymaslov.inspector.View.IView;

/**
 * Created by Администратор on 08.04.2016.
 */
public interface IPresenter{
    void bindActivity(IView view);
    void unbindActivity();
}
