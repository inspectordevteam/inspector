package ru.sergeymaslov.inspector;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.sergeymaslov.SyncCommands;

import ru.sergeymaslov.inspector.Annotations.Sync;
import ru.sergeymaslov.inspector.BaseClasses.IName;
import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.InspectorDatabase;
import ru.sergeymaslov.inspector.Interfaces.Syncable;
import ru.sergeymaslov.inspector.R;

/**
 * Created by Администратор on 27.02.2016.
 */
@Table(database = InspectorDatabase.class)
public class Violation extends BaseModel implements IName, Syncable {

    @Sync
    @PrimaryKey
    public long eventId;

    @Sync
    @PrimaryKey
    public String violationText;

    @Sync
    @Column
    public String comment;

    public Violation(){
        violationText = "Violation";
    }

    @Override
    public String getObjectName() {
        return violationText;
    }

    public long
    getEventId() {
        return eventId;
    }

    public String getViolationText() {
        return violationText;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public String getMessage() {
        return InspectorApplication.getInstance().getString(R.string.violation_sync_caption);
    }

    @Override
    public void onSyncFinished() {

    }

    @Override
    public String getPath() {
        return "save-violations";
    }
}
