package ru.sergeymaslov.inspector.View;

/**
 * Created by Maslov on 20.04.2016.
 */
public interface ILauncherView extends IView {
    void onDbCopyToSDCardFailed();
    void onDbCopyToInnerStorageFailed();
    void onDbCopySuccess();
}
