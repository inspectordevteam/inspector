package ru.sergeymaslov.inspector.View;

import java.util.List;

import ru.sergeymaslov.inspector.Interface.Lists.EventListItem;

/**
 * Created by Администратор on 08.04.2016.
 */
public interface IEventHistoryView extends IView{
    void refreshAdapter();
    void onItemsLoaded(List<EventListItem> list);
}
