package ru.sergeymaslov.inspector.View;

import android.app.DialogFragment;

import java.util.ArrayList;

import ru.sergeymaslov.inspector.Dialogs.Dialog;
import ru.sergeymaslov.inspector.Interface.DataManagers.NewEventDataManager;
import ru.sergeymaslov.inspector.Interface.Lists.ValueChooserListItem;

/**
 * Created by Администратор on 11.04.2016.
 */
public interface INewEventView extends IView {
    NewEventDataManager getDataManager();
    void onItemsRefresh(ArrayList<ValueChooserListItem> items);
    void showDialog(DialogFragment dialog);
    void showViolationCommentsDialog();
    String getString(int resId);
    void gotoPhotos();
}
