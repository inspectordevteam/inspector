package ru.sergeymaslov.inspector.Exceptions;

/**
 * Created by Администратор on 08.03.2016.
 */
public class EventSaveNoDataException extends Exception {
    public EventSaveNoDataException(String message){
        super(message);
    }
}
