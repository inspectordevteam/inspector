package ru.sergeymaslov.inspector.Exceptions;

/**
 * Created by Администратор on 18.03.2016.
 */
public class SyncFailedException extends Exception {

    public SyncFailedException(String detailMessage) {
        super(detailMessage);
    }
}
