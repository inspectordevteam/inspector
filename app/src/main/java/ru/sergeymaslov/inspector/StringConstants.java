package ru.sergeymaslov.inspector;

/**
 * Created by Администратор on 28.02.2016.
 */
public class StringConstants {
    public static final String EMPTY = "";
    public static final String SPACE = " ";
    public static final String UNDERLINE = "_";
    public static final String NEWLINE = "\n";
    public static final String FILE_TYPE_EVERY_FILE = "*/*";
    public static final String FILE_TYPE_IMAGES = "image/*";
}
