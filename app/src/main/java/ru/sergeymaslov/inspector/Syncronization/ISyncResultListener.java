package ru.sergeymaslov.inspector.Syncronization;

/**
 * Created by Администратор on 21.05.2016.
 */
public interface ISyncResultListener {
    void onSyncSuccess();
    void onSyncFailed();
}
