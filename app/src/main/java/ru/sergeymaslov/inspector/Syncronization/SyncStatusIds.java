package ru.sergeymaslov.inspector.Syncronization;

/**
 * Created by Yarkov on 3/14/2016.
 */
public class SyncStatusIds {
    public static final int STATUS_RECEIVED = 1;
    public static final int STATUS_NEW = 2;
    public static final int STATUS_SENT = 3;
}
