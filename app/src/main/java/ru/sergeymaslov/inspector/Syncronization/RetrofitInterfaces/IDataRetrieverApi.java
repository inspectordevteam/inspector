package ru.sergeymaslov.inspector.Syncronization.RetrofitInterfaces;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import ru.sergeymaslov.inspector.Car;
import ru.sergeymaslov.inspector.Event;
import ru.sergeymaslov.inspector.Violation;
import rx.Observable;

/**
 * Created by Maslov on 28.04.2016.
 */
public interface IDataRetrieverApi {

    @GET("")
    Observable<List<Event>> getEvents();

    @GET("/androidApi/main/get-{name}")
    Call<ResponseBody> getData(@Path("name") String name, @QueryMap Map<String, String> argsMap);

    @GET("")
    Observable<List<Violation>> getViolations();

    @GET("")
    Observable<List<Car>> getCars();

}
