package ru.sergeymaslov.inspector.Syncronization.RetrofitInterfaces;


import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Maslov on 28.04.2016.
 */
public interface IDataSyncApi {
    @FormUrlEncoded
    @POST("androidApi/main/{path}")
    Call<ResponseBody> send(@Path("path") String path, @FieldMap Map<String, String> resultData);

    @FormUrlEncoded
    @POST("androidApi/main/save-drivers")
    Call<ResponseBody> sendDrivers(@FieldMap Map<String, String> resultData);

    @FormUrlEncoded
    @POST("androidApi/main/save-events")
    Call<ResponseBody> sendEvents(@FieldMap Map<String, String> resultData);

    @FormUrlEncoded
    @POST("androidApi/main/save-violations")
    Call<ResponseBody> sendViolations(@FieldMap Map<String, String> resultData);

    @GET("modules/androidApi/controllers/mainController.php")
    void sendPhotos();

}
