package ru.sergeymaslov.inspector.Syncronization;

import android.util.Log;
import android.util.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.sergeymaslov.inspector.Annotations.Sync;
import ru.sergeymaslov.inspector.Driver;
import ru.sergeymaslov.inspector.Exceptions.SyncFailedException;
import ru.sergeymaslov.inspector.Interface.DataManagers.SyncProcessManager;
import ru.sergeymaslov.inspector.Interfaces.ISyncProgressListener;
import ru.sergeymaslov.inspector.Interfaces.Syncable;
import ru.sergeymaslov.inspector.LastSyncDate;
import ru.sergeymaslov.inspector.Syncronization.RetrofitInterfaces.IDataRetrieverApi;
import ru.sergeymaslov.inspector.Syncronization.RetrofitInterfaces.IDataSyncApi;
import ru.sergeymaslov.inspector.Tools.DateTools;

/**
 * Created by Администратор on 24.03.2016.
 */
public class DataSync {
    private final String TAG = "DataSync";
    private static final String KEY = "data";
    private ArrayList<ArrayList<? extends Syncable>> mSyncObjects;
    private ISyncProgressListener mSyncProgressListener;
    private Gson mGson;
    private Retrofit mRetrofit;
    private SyncProcessManager mDataManager;
    IDataRetrieverApi mDataRetrieverApi;
    IDataSyncApi mDataSendApi;
    Callback<ResponseBody> mDataSendResponseCallback;
    Callback<ResponseBody> mDataRequestResponseCallback;
    private boolean isTransactionSuccessful;

    //GET request keys
    public static final String KEY_NAME = "name";
    public static final String KEY_LAST_SYNC_DATE = "last_sync_date";
    public static final String KEY_DRIVER = "drivers";
    public static final String KEY_VIOLATION = "violations";

    public DataSync() {
        mDataManager = SyncProcessManager.getInstance();
        mGson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .setLenient()
                .create();
        mRetrofit = new Retrofit.Builder()
                .baseUrl("http://inspector.zapto.org/")
                .addConverterFactory(GsonConverterFactory.create(mGson))
                .build();
        mDataRetrieverApi = mRetrofit.create(IDataRetrieverApi.class);
        mDataSendApi = mRetrofit.create(IDataSyncApi.class);
        initCallbacks();
    }

    public void setSyncProgressListener(ISyncProgressListener syncProgressListener){
        mSyncProgressListener = syncProgressListener;
    }

    public void setObjectsToSend(ArrayList<ArrayList<? extends Syncable>> syncObjects){
        mSyncObjects = syncObjects;
    }

    private void initCallbacks(){
        mDataSendResponseCallback = new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null){
                        isTransactionSuccessful = true;
                        Log.d(TAG, "Transaction successful " + response.body().string());
                    }
                    if (response.errorBody() != null){
                        isTransactionSuccessful = false;
                        Log.d(TAG, "Error occurred " + response.errorBody().string());
                    }
                } catch (IOException e) {
                    isTransactionSuccessful = false;
                    e.printStackTrace();
                }
                Log.d(TAG, "Response, code " + String.valueOf(response.code()));
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isTransactionSuccessful = false;
                Log.d(TAG, "Failure");
                t.printStackTrace();
            }
        };
        mDataRequestResponseCallback = new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    try {
                        ArrayList<LinkedTreeMap<String, String>> responseData = mGson.fromJson(response.body().string(), new TypeToken<List<Map<String, String>>>(){}.getType());
                        handleReceivedData(responseData);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        };
    }

    public void sync() throws IOException, SyncFailedException {
        for (ArrayList<? extends Syncable> objects : mSyncObjects) {
            if (!objects.isEmpty()) {
                sync(objects);
            }
        }
    }

    private void sync(ArrayList<? extends Syncable> objects) throws SyncFailedException, IOException {
        if (objects.isEmpty()){
            return;
        }
        if (!isTransactionSuccessful){
            mSyncProgressListener.onProgressUpdate(new Pair<>(SynchronizationTask.COMMAND_SYNC_FAILED, null));
        }
        mSyncProgressListener.onProgressUpdate(new Pair<>(SynchronizationTask.COMMAND_SET_PROGRESS, 0));
        mSyncProgressListener.onProgressUpdate(new Pair<>(SynchronizationTask.COMMAND_SET_MAX_PROGRESS, objects.size()));
        try {
            HashMap<String, String> packageToSend;
            ArrayList<HashMap<String, String>> finalList = new ArrayList<>();
            for (Syncable syncObject : objects) {
                packageToSend = new HashMap<>();
                mSyncProgressListener.onProgressUpdate(new Pair<>(SynchronizationTask.COMMAND_SET_PROGRESS_MESSAGE, syncObject.getMessage()));
                Class syncObjectClass = syncObject.getClass();
                Field[] fields = syncObjectClass.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Sync.class)) {
                        for (Method method : syncObjectClass.getDeclaredMethods()) {
                            if (method.getName().startsWith("get")
                                    && method.getName().toLowerCase().contains(field.getName().toLowerCase())
                                    && method.getName().length() == (field.getName().length() + 3)) {
                                Object fieldValue = method.invoke(syncObject, new Object[0]);
                                packageToSend.put(field.getName(), String.valueOf(fieldValue));
                            }
                        }
                    }
                }
                finalList.add(packageToSend);
                mSyncProgressListener.onProgressUpdate(new Pair<>(SynchronizationTask.COMMAND_UPDATE_PROGRESS, 1));
                syncObject.onSyncFinished();
            }
            Map<String, String> resultData = new HashMap<>();
            resultData.put(KEY, mGson.toJson(finalList));
            Call<ResponseBody> response = mDataSendApi.send(objects.get(0).getPath(), resultData);
            response.enqueue(mDataSendResponseCallback);
        } catch (InvocationTargetException ite) {
            Log.e(TAG, "Possibly wrong getter method has been invoked!");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void requestData(ArrayList<String> keys){
        for (String key : keys){
            requestData(key);
        }
    }

    private void requestData(String key){
        Date lastSyncDate = mDataManager.getLastSyncDateFor(key);
        Map<String, String> args = new HashMap<>();
        args.put(KEY_LAST_SYNC_DATE, DateTools.dateToString(lastSyncDate, null));
        Call<ResponseBody> response = mDataRetrieverApi.getData(key, args);
        response.enqueue(mDataRequestResponseCallback);
    }

    private void handleReceivedData(ArrayList<LinkedTreeMap<String, String>> response){
        String receivedDataType = response.get(0).get("content");
        switch (receivedDataType){
            case KEY_DRIVER:{
              for (LinkedTreeMap<String, String> drivers : response) {
                  saveDriver(drivers);
              }
            }
        }
        saveLastSyncDate(receivedDataType);
    }

    private void saveDriver(LinkedTreeMap<String, String> driverData){
        if (driverData.get("content") != null){
            return;
        }
        Driver driver = new Driver();
        driver.driverId = Long.parseLong(driverData.get("driverId"));
        driver.name = driverData.get("name");
        driver.surname = driverData.get("surname");
        driver.lastName = driverData.get("lastName");
        driver.driverLicenseNumber = driverData.get("licenseNumber");
        driver.save();
    }

    private void saveLastSyncDate(String key){
        LastSyncDate lastSyncDate = new LastSyncDate();
        lastSyncDate.setName(key);
        lastSyncDate.setDate(new Date());
        lastSyncDate.save();
    }

}
