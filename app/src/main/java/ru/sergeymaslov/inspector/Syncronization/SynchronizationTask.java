package ru.sergeymaslov.inspector.Syncronization;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Pair;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


import ru.sergeymaslov.inspector.BaseClasses.InspectorApplication;
import ru.sergeymaslov.inspector.Dialogs.Dialog;
import ru.sergeymaslov.inspector.Dialogs.DialogManager;
import ru.sergeymaslov.inspector.Event;
import ru.sergeymaslov.inspector.Exceptions.SyncFailedException;
import ru.sergeymaslov.inspector.FileTools;
import ru.sergeymaslov.inspector.Interface.DataManagers.SyncProcessManager;
import ru.sergeymaslov.inspector.Interfaces.ISyncProgressListener;
import ru.sergeymaslov.inspector.Interfaces.Syncable;
import ru.sergeymaslov.inspector.R;

/**
 * Created by Администратор on 17.03.2016.
 */
public class SynchronizationTask extends AsyncTask<Void, Pair<Integer, Object>, Void> {
    private final String TAG = "SyncTask";
    private ProgressDialog progressDialog;
    private SyncProcessManager mDataManager;
    private ISyncProgressListener mSyncProgressListener;
    private ISyncResultListener mSyncResultListener;

    public static final int COMMAND_SET_MAX_PROGRESS = 0;
    public static final int COMMAND_UPDATE_PROGRESS = 1;
    public static final int COMMAND_SET_PROGRESS_MESSAGE = 2;
    public static final int COMMAND_SYNC_FAILED = 3;
    public static final int COMMAND_SET_PROGRESS = 4;

    public SynchronizationTask(ProgressDialog dialog, SyncProcessManager dataManager, ISyncResultListener syncResultListener) {
        progressDialog = dialog;
        mDataManager = dataManager;
        mSyncResultListener = syncResultListener;
        initListeners();
    }

    private void initListeners() {
        mSyncProgressListener = progress -> publishProgress(progress);
    }

    //NOTE: we should sync events after we synced all event-dependant classes because of event's sync status field
    @Override
    protected Void doInBackground(Void... params) {
        try {
            sendData();
            receiveData();
        } catch (IOException ioe) {
            mSyncResultListener.onSyncFailed();
            ioe.printStackTrace();
        } catch (SyncFailedException e) {
            mSyncResultListener.onSyncFailed();
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Pair<Integer, Object>... values) {
        int commandId = values[0].first;
        switch (commandId) {
            case COMMAND_SET_MAX_PROGRESS: {
                int maxProgress = (int) values[0].second;
                progressDialog.setMax(maxProgress);
                break;
            }
            case COMMAND_UPDATE_PROGRESS: {
                int progress = (int) values[0].second;
                progressDialog.incrementProgressBy(progress);
                break;
            }
            case COMMAND_SET_PROGRESS_MESSAGE: {
                String updateMessage = (String) values[0].second;
                progressDialog.setMessage(updateMessage);
                break;
            }
            case COMMAND_SYNC_FAILED: {
                progressDialog.setMessage(getString(R.string.sync_failed_message));
                progressDialog.setProgress(0);
                progressDialog.setCancelable(true);
                break;
            }
            case COMMAND_SET_PROGRESS: {
                int progress = (int) values[0].second;
                progressDialog.setProgress(progress);
                break;
            }
        }
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        progressDialog.dismiss();
        progressDialog = null;
        mSyncResultListener.onSyncSuccess();
        super.onPostExecute(aVoid);
    }

    private void sendData() throws IOException, SyncFailedException {
        ArrayList<ArrayList<? extends Syncable>> objectsToSync = new ArrayList<>();
        objectsToSync.add(mDataManager.getEvents());
        objectsToSync.add(mDataManager.getAllViolations());
        objectsToSync.add(mDataManager.getUnsyncedDrivers());
        DataSync dataSync = new DataSync();
        dataSync.setObjectsToSend(objectsToSync);
        dataSync.setSyncProgressListener(mSyncProgressListener);
        dataSync.sync();
    }

    private void receiveData() {
        DataSync dataSync = new DataSync();
        ArrayList<String> keys = new ArrayList<>();
        keys.add(DataSync.KEY_DRIVER);
        dataSync.requestData(keys);
    }

    //TODO create a new class, say, file synchronizer and move this there
    private void syncEventPhotos() throws SyncFailedException, IOException {
        ArrayList<Event> events = mDataManager.getEvents();
        publishProgress(new Pair<>(COMMAND_SET_PROGRESS_MESSAGE, getString(R.string.photos_sync_caption)));
        for (Event event : events) {
            File[] eventPhotos = mDataManager.getEventPhotos(event);
            ArrayList<byte[]> photosBytes = new ArrayList<>();
            for (int i = 0; i < eventPhotos.length; i++) {
                byte[] photo = FileTools.convertFileByteArray(eventPhotos[i]);
                if (!photosBytes.contains(photo)) {
                    photosBytes.add(photo);
                }
            }
            HashMap<Integer, Object> packageToSend = new HashMap<>(eventPhotos.length);
            for (byte[] file : photosBytes) {
                packageToSend.put(0, event.eventId);
                packageToSend.put(1, file);
            }
        }
    }

    private String getString(int resId) {
        return InspectorApplication.getInstance().getString(resId);
    }
}
