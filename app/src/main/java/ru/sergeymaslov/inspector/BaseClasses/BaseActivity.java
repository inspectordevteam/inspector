package ru.sergeymaslov.inspector.BaseClasses;


import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import butterknife.ButterKnife;

/**
 * Created by Администратор on 28.02.2016.
 */
public abstract class BaseActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    public static final String DIALOG = "dialog";

    protected static final int MY_PERMISSIONS_REQUEST_STORAGE = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getViewId());
        ButterKnife.bind(this);
        mRecyclerView = (RecyclerView) findViewById(android.R.id.list);
        if (mRecyclerView != null) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    protected abstract int getViewId();

    protected RecyclerView getRecyclerView(){
        return mRecyclerView;
    }

    protected void setAdapter(RecyclerView.Adapter adapter){
        if (mRecyclerView != null){
            mRecyclerView.setAdapter(adapter);
        }
    }
    public boolean checkPermissions(String permission, int permissionCode, int sdkVersion) {
        if (Build.VERSION.SDK_INT >= sdkVersion) {
            int permissionCheckWrite = ContextCompat.checkSelfPermission(this,
                    permission);
            if (permissionCheckWrite != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{permission},
                        permissionCode);
                return false;
            }
        }
        return true;
    }
}
