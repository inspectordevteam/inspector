package ru.sergeymaslov.inspector.BaseClasses;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Environment;

import com.raizlabs.android.dbflow.config.FlowManager;

import java.io.IOException;
import java.util.Date;

import ru.sergeymaslov.inspector.Model.DataManager;
import ru.sergeymaslov.inspector.Model.DatabaseDataProvider;
import ru.sergeymaslov.inspector.InspectorDatabase;
import ru.sergeymaslov.inspector.FileTools;
import ru.sergeymaslov.inspector.IntConstants;
import ru.sergeymaslov.inspector.LastSyncDate;
import ru.sergeymaslov.inspector.StringConstants;
import ru.sergeymaslov.inspector.Syncronization.DataSync;


public class InspectorApplication extends Application {
    private static InspectorApplication app;
    private String databaseInternalPath;
    private String databaseSdCardPath;
    private DataManager mDataManager;


    private final String GLOBAL_PREFS = "myprefs";

    public static final String DATABASE_FILE_NAME = InspectorDatabase.NAME + ".db";

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        databaseSdCardPath = Environment.getExternalStorageDirectory().toString() + "/" + DATABASE_FILE_NAME;
        databaseInternalPath = super.getDatabasePath(DATABASE_FILE_NAME).getPath();
        mDataManager = new DataManager(new DatabaseDataProvider());

        FlowManager.init(this);

        initializeDefaultDatabaseSettings();
    }

    public DataManager getDataManager(){
        return mDataManager;
    }

    public static InspectorApplication getInstance() {
        return app;
    }

    public void copyDBToSDCard() throws IOException {
        FileTools.copy(databaseInternalPath, databaseSdCardPath);
    }

    public String getDatabaseInternalPath() {
        return databaseInternalPath;
    }

    public String getDatabaseSdCardPath() {
        return databaseSdCardPath;
    }

    public void copyDBFromSDCard() throws IOException {
        FileTools.checkIfDatabaseFolderExists();
        FileTools.checkIfDatabaseFileExists();
        FileTools.copy(databaseSdCardPath, databaseInternalPath);
    }

    public void saveStringToSharedPreferences(String key, String data){
        SharedPreferences prefs = getSharedPreferences(GLOBAL_PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, data);
        editor.commit();
    }

    public void saveIntegerToSharedPreferences(String key, Integer data){
        SharedPreferences prefs = getSharedPreferences(GLOBAL_PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, data);
        editor.commit();
    }

    public String getStringFromSharedPreferences(String key){
        SharedPreferences prefs = getSharedPreferences(GLOBAL_PREFS, 0);
        return prefs.getString(key, StringConstants.EMPTY);
    }

    public Integer getIntegerFromSharedPreferences(String key){
        SharedPreferences prefs = getSharedPreferences(GLOBAL_PREFS, 0);
        return prefs.getInt(key, IntConstants.DEFAULT_ZERO);
    }

    private void initializeDefaultDatabaseSettings(){
        Date driverSyncDate = mDataManager.getDataProvider().getLastSyncDateFor(DataSync.KEY_DRIVER);
        Date violationSyncDate = mDataManager.getDataProvider().getLastSyncDateFor(DataSync.KEY_VIOLATION);
        if (driverSyncDate == null){
            LastSyncDate lastSyncDate = new LastSyncDate();
            lastSyncDate.setName(DataSync.KEY_DRIVER);
            lastSyncDate.setDate(new Date());
            lastSyncDate.save();
        }
        if (violationSyncDate == null){
            LastSyncDate lastSyncDate = new LastSyncDate();
            lastSyncDate.setName(DataSync.KEY_VIOLATION);
            lastSyncDate.setDate(new Date());
            lastSyncDate.save();
        }
    }
}
