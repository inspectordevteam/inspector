package com.sergeymaslov;

/**
 * Created by Администратор on 17.03.2016.
 */
public class SyncCommands {

    //send commands ids start with 200
    public static class SendCommands {
        public static final int EVENTS = 200;
        public static final int EVENT_VIOLATIONS = 201;
        public static final int OBJECT_COUNT = 202;
        public static final int EVENT_PHOTOS = 203;
        public static final int DRIVERS = 204;
        //TODO remove, temporary solution!
        public static final int CARS = 205;
    }

    public static class SpecialCommands{
        public static final int SYNC_STOP_COMMAND = 666;
    }

    public static class Response{
        public static final int RESPONSE_OK = 100;
        public static final int RESPONSE_ERROR_UNKNOWN = 99;
    }
}
