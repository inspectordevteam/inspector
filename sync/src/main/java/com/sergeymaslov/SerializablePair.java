package com.sergeymaslov;

import java.io.Serializable;

/**
 * Created by Администратор on 18.03.2016.
 */
public class SerializablePair<T1, T2> implements Serializable{
    private static final long serialVersionUID = -6193662206506603367L;
    private T1 firstValue;
    private T2 secondValue;

    public SerializablePair(T1 first, T2 second){
        firstValue = first;
        secondValue = second;
    }

    public T1 getFirstValue() {
        return firstValue;
    }

    public T2 getSecondValue() {
        return secondValue;
    }
}
